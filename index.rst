
.. _onestlatech:

====================================================================
**OnEstLaTech**
====================================================================

.. figure:: _static/1500x500.webp
   :align: center


.. seealso::

   - https://github.com/onestlatech
   - https://discord.gg/se3PnEr
   - https://mastodon.social/@onestlatech
   - https://onestla.tech/
   - https://ag.onestla.tech/explore?order=memberships_count


.. sidebar:: Doc onestla.tech

    :Date: |version|
    :AuteurE: **UnE scribe**



.. toctree::
   :maxdepth: 5

   manifeste/manifeste
   ethique/ethique
   reunions/reunions
   chantiers/chantiers

.. toctree::
   :maxdepth: 6

   ressources/ressources

.. toctree::
   :maxdepth: 5

   syndicats/syndicats
   glossaire/glossaire
   index/index
   meta/meta
