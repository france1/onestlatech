

.. index::
   pair: glossaire ; onestla.tech


.. _glossaire_onestla:

==========================
Glossaire
==========================

.. seealso::

   - :ref:`glossaire_juridique`
   - https://www.leslibraires.fr/livre/17478081-la-guerre-des-mots-combattre-le-discours-poli--derkaoui-selim-le-passager-clandestin
   - https://www.frustrationmagazine.fr/do-you-speak-bullshit-english-comment-langlais-dentreprise-renforce-la-classe-dominante-partie-i/


.. glossary::
   :sorted:

   action directe
   Action directe
       L’action directe c’est l’idée toute simple que ce sont les personnes
       directement concernées qui agissent en définissant, menant et
       contrôlant collectivement leur mouvement.voir :term:`démocratie directe`
       Nous devons réaffirmer, loin de tout radicalisme contemporain voulant
       faire de l'action directe un mode d'action violent, l’action directe
       comme une pratique syndicale où ce sont les travailleurs eux-mêmes qui
       interviennent directement dans leur lutte, à tous les niveaux et à
       toutes les étapes, sans recours aucun à des spécialistes de la
       représentation et de la négociation.


   adelphique
   adelphiques
   Adelphique
   Adelphiques
       De relation fraternelle et/ou sororale.


   Adoption d'une motion
       Le "quorum" pour qu'un congès confédéral soit valide est de la moitié
       des syndicats de la CNT à jour de cotisations au 4e mois précédent le
       Congrès. Une motion est adoptée si au moins la moitié des syndicats
       présents au Congrès vote pour.

   amendement
   Amendement
       Sert à apporter une modification à une :term:`motion`. Il doit porter
       sur la proposition débattue et doit être proposé et appuyé. A noter
       qu'un amemdement doit être en principe en accord avec la motion et ne
       vouloir changer qu'un détail (le sens de la motion doit demeurer le même).
       Les amendements sont rédigés lors du vote des motions en AG de syndicat.
       Des amendements peuvent être proposés lors du Congrès au regard des
       débats et des différentes propositions des syndicats.


   Anarcho-syndicalisme
       syndicalisme se réclamant de l'anarchisme dans les moyens et la finalité.
       Proche du :term:`Syndicalisme révolutionnaire`, mais pas strictement.


   Ateliers
       Les ateliers sont des moments où les délégués se répartissent pour
       travailler autour de thèmes et produire du matériel (textes, affiches,
       slogans...). Ces travaux sont préparés en séance plénière avant l'atelier.
       Les ateliers rendent ensuite compte de leur travail en séance plénière
       pour adoption.


   A.G
   AG
   assemblée générale
   Assemblée Générale
   Assemblées générales
       Réunion des adhérents du syndicat destiné à la prise de décision
       collective visant à élaborer les orientations et actions du syndicat
       (ou de la structure correspondante).


   ASA (Autorisation Syndicale d'Absence)
       eiste dans certains services publics, ces autorisations d'absence sont
       définies par l'article 13 du décret 82/447. Elles donnent droit à 10 jours
       d'absence pour participation aux congrès syndicaux. La demande doit être
       faite en général un mois à l'avance (même si certains textes parlent de
       8 jours). Il est possible de demander des jours de plus en plus des dates
       du congrès pour le temps de trajet.


   autogestion
   Autogestion
   autogestionnaire
   Autogestionnaire
       En partant du principe que l’autogestion sera le moteur de la société
       future et que **celle-ci n’est pas innée**, mais s’apprend, se pratique
       et se confronte à la réalité, nous devons la mettre au centre de nos
       dynamiques et de nos initiatives.

   A.S
       Anarcho-syndicalisme


   ASSR
   AS & SR
       Anarcho-syndicalisme et Syndicalisme Révolutionnaire

   Autonomie d'action
       Notre autonomie d’action s’exprime principalement au travers des
       principes de :term:`démocratie directe` et d’:term:`action directe` revendiqués comme
       pratique syndicale


   commissions
   Commission
       Définition 1
           Prévues dans les statuts confédéraux, elles peuvent être de 2 types:

           - des commissions chargées d'un mandat entre 2 congrès
             (exemple: commission Statuts, commission juridique)
           - des commissions ponctuelles de Congrès (vérification des comptes,
             synthèse de motions, péréquation...)

       Définition 2
           Réunion de personnes chargées d'étudier une question, de contrôler des
           comptes, un mandat, etc.
           Au sein d'un syndicat, elle est constituée par des adhérents travaillant
           sur un thème précis.


   cornucopianisme
       Croyance en des ressources illimitées et au fait que des innovations
       permanentes permettront toujours de résoudre les problèmes rencontrés
       par l’humanité (épuisement des ressources).

       :source: https://fr.wiktionary.org/wiki/cornucopianisme

   commutatif
   Commutatif
   commutatifs
       Convention par laquelle chacune des parties s'engage à donner ou à
       exécuter une chose considérée comme l'équivalent de ce qu'on lui donne
       ou de ce que l'on fait pour elle.

   C.E
   CE
   Comité d'Entreprise
       S'agissant du comité d'entreprise (CE), celui-ci n'ayant de pouvoir de
       décision  qu'en matière de gestion des œuvres sociales et ne donnant
       que des avis au  patron dans la gestion de l'entreprise, est
       l'institution type de collaboration  de classes que rejette la CNT,
       d'autant plus que ses membres sont élus sans mandat précis et sont
       irrévocables pendant deux ans.


   Charte d'Amiens
       adoptée en 1906 par la CGT dans un congrès. Ce compromis entre anarchistes
       et socialistes stipule que "la CGT groupe, en dehors de toute école politique,
       tous les travailleurs conscients de de la lutte à mener pour la disparition
       du salariat et du patronat"


   Compte rendu d'activité des mandaté(e)s
       Les congrès sont l'occasion, en ouverture, de rendre les mandats.
       Chaque mandaté propose, si possible par écrit, un bilan de son mandat.
       Les adhérents décident alors de lui donner ou non le :term:`quitus`.

   Cotisation
       L’engagement syndical se traduit par un premier acte militant:
       s’acquitter de sa cotisation.

   CP
   Communiqué de presse
       Communiqué de presse.


   contre-motion
   contre-motions
   contre motion
       Une contre-motion constitue une proposition qui entre en contradiction
       avec le sens de la motion initiale.

       Elle n'est discutée et soumise au vote que si la motion à laquelle
       elle s'oppose n'est pas adoptée.


   congrès
   Congrès
   Congrès confédéral
       Réunion statutaire des délégués. Il se déroule tous les deux ans.

   Déléguée(e)s
       Les syndicats envoient des délégués aux divers congrès et réunions de
       structure (U.L, U.R, fédérations, etc.) pour les représenter et porter leurs
       mandats sur les motions. Les délégués s'expriment au nom de leur syndicat
       et non en leur nom propre.

   démocratie directe
   Démocratie directe
       Notre autonomie d’action s’exprime principalement au travers des
       principes de démocratie et d’action directe revendiqués comme pratique
       syndicale. La pratique de l’:term:`action directe` comme mode d’action n’est que
       le prolongement des conceptions de démocratie directe.
       Le moteur interne de notre fonctionnement à tous les niveaux doit être
       la pratique de la démocratie directe.

   DS
   D.S
       Délégué(e) Syndical

   Entraide
       facteur et condition de l'évolution sociale.

   Ethique
       est à la morale ce que l'anarchie est au gouvernement, fondée sur
       l'immanence et non la transcendance des principes.

   Illectronisme
       L’illectronisme, inhabileté numérique, illettrisme numérique, ou
       encore illettrisme électronique, est la difficulté, voire l'incapacité,
       que rencontre une personne à utiliser les appareils numériques et
       les outils informatiques en raison d'un manque ou d'une absence
       totale de connaissances à propos de leur fonctionnement.

       Le terme illectronisme transpose le concept d’illettrisme dans le
       domaine de l’informatique.

       :source: https://fr.wikipedia.org/wiki/Illectronisme


   Exploité
       Au plan individuel, il n'est pas nécessaire au départ d'être
       révolutionnaire pour adhérer à la C.N.T. Notre but est d'attirer le plus
       grand nombre de travailleurs dont la caractéristique commune est
       *le statut d'exploité*.


   mandat
   mandats
   Mandat
       2 sens:

       - les tâches prises en charge par un(e) mandaté(e) (ex: mandat de trésorier)
       - ou bien les décisions du syndicat portées par le délégué aux diverses
         réunions statutaires (les votes des motions, les :term:`quitus`...)

   Mandatements
       Il existe différents types de mandats que le syndicat peut confier à
       son/sa délégué(e):

       - Le :term:`mandat` **ferme** (ou **fermé**)
         Le syndicat s’est réuni en AG et a pris position de manière ferme sur
         un certain nombre de points, il mandate un(e) de ses syndiqué(e)s pour
         faire part de ses décisions et travailler à la réalisation de ses
         objectifs.
         Le ou la mandaté(e) a alors une mandat très clair qui a été entièrement
         planifié par l’AG du syndicat.

         + **Avantages** : le travail de la ou du mandaté(e) est grandement
           facilité, elle ou il n’a pas de souci à avoir quant à son respect
           des positions de son syndicat.
         + **Inconvénients** : il n’y a pas de place pour la discussion, si
           tous les mandats étaient de ce type, nous n’aurions plus besoin de
           nous déplacer pour un congrès, il suffirait de voter par
           correspondance sur les motions.

       - Le :term:`mandat` **semi ouvert**
         Le syndicat s’est réuni en AG et a pris position, mais il laisse une
         marge de manœuvre à sa ou son mandaté(e) afin de réagir aux discussions
         qui ont lieu lors du congrès. Le ou la mandaté(e) connait parfaitement
         la position de son syndicat qui lui a laissé une possibilité
         d’interprétation de ses positions.

         + **Avantages** : le syndicat se garde une possibilité de réagir aux
           échanges par la voix de son ou sa mandaté(e)
         + **Inconvénients** : la ou le mandaté(e) doit être particulièrement
           vigilant(e) lors de la réunion afin de respecter scrupuleusement
           son mandat

       - Le :term:`mandat` **ouvert**
         Une AG du syndicat a eu lieu durant laquelle les membres se sont
         exprimés autour des sujets relevant du mandat, mais il n’y a pas eu
         de décisions fermes. Le ou la mandaté(e) doit alors faire des choix en
         essayant de respecter «l’esprit» du syndicat.

         + **Avantages** : le syndicat peut parfois gagner beaucoup de temps en
           évitant par exemple de discuter chaque motion et en les regroupant
           par thème afin d’avoir une discussion sur l’ensemble de celles-ci.
         + **Inconvénients** : la tâche de la ou du mandaté(e) peut devenir
           plus difficile car il n’est pas toujours aisé de traduire des prises
           de position générale en décisions précises.

       - Le :term:`mandat` **en blanc**
         Il n'y a pas eu de discussions au sien du syndicat et celui-ci fait
         entièrement confiance à son ou sa mandaté(e) pour le représenter et
         prendre des décisions.

         + **Avantages** : le gain de temps est énorme et peut permettre à un
           syndicat de continuer à "fonctionner" lorsque celui-ci traverse une
           période difficile/
         + **Inconvénients** : aucune discussion n'a lieu au sein du syndicat
           autour des points traités et donc pas d'auto-formation des
           syndiqué(e)s, de plus ils ou elles risquent fort que le ou la
           délégué(e) ne représente que lui/elle même.

       Quel que soit le type de mandatement, le ou la mandaté(e) devra répondre
       de ses décisions devant son syndicat.

   motions
   motion
   Motion
       Proposition sous forme de texte définissant avec précision son objectif
       et les modalités de sa mise en application.
       Une motion est généralement appuyée et expliquée par un argumentaire distinct.

   Ordre
   ordre
       Dans le sens de "Ordre des ingénieurs", "Ordre des médecins"
       La différence entre un ordre et un syndicat sont que les ordres
       sont incorporés au pouvoir existant (pouvoir réglementaire reconnu
       par l’État) alors que les syndicats sont censé être des contre-pouvoirs.

       :source: https://discordapp.com/channels/654441797539594277/771806119232274474/773138691165257768


   Ordre du jour
   ordre du jour
       L’:term:`assemblée générale` d'un syndicat 'est préparée par l’envoi à
       chaque adhérent d’un ordre du jour proposé, que **chacun peut compléter**.


   Présidence de séance
       Un(e) ou pluisieurs (c'est mieux !) déléguée(s) prennent en charge la
       présidence de séance pour une partie
       (une demi-journée en général).
       Il s'agit de faciliter le déroulement de la réunion.

       La présidence rappelle **l'ordre du jour** et les éventuelles modifications.

       - accorde le droit de parole / dirige l'Assemblée au niveau des
         procédures et des discussions
       - rappelle à l'ordre tout membre qui ne respecte pas l'ordre des
         procédures et des discussions
       - décide des points d'ordre
       - procède au relevé des décisions qui émergent des discussions pour
         éventuellemement les soumettre au vote après s'être assurée qu'elles
         sont comprises par tous et toutes.

   Quitus
   quitus
       Acte qui arrête un compte/un mandat et qui reconnaît l'exactitude de la gestion
       de celui qui le tenait.
       Chaque mandat au sein du syndicat est validé par le vote d'un quitus en
       congrès de syndicat.


   quorum
   Quorum
       En droit, le quorum est le nombre minimal de membres d'un corps
       délibératif nécessaire à la validité d'une décision. C'est souvent la
       moitié des membres, mais beaucoup d'entités ont un pré requis plus bas
       ou plus haut.
       Lorsque le quorum n'est pas atteint, le corps délibératif ne peut pas
       tenir de vote et ne peut pas changer le statu quo. Ainsi, les votants
       en faveur du statu quo peuvent bloquer une décision en ne se présentant
       pas au vote. Le vote sera alors automatiquement rejeté et le
       statu quo conservé.

       .. seealso::

           - http://fr.wikipedia.org/wiki/Quorum

   R.S.S
       Représentant de Section Syndicale


   Séance plénière
       Moment où l'ensemble des délégué(e)s se retrouvent ensemble (par opposition
       aux temps de travail en commissions).

   Sociocratie
   sociocratie
       La sociocratie est un mode de gouvernance partagée qui permet à
       une organisation, quelle que soit sa taille, de fonctionner
       efficacement selon un mode auto-organisé caractérisé par des
       prises de décision distribuées sur l'ensemble de la structure.

       Son fondement moderne est issu des théories systémiques et date
       de 1970.
       La sociocratie s'appuie sur la liberté et la co-responsabilisation
       des acteurs. Dans une logique d'auto-organisation faisant confiance
       à l'humain, elle va mettre le pouvoir de l'intelligence collective
       au service du succès d'objectifs communs.
       Cette approche permet donc d’atteindre ensemble un objectif
       partagé, dans le respect des personnes, en préservant la diversité
       des points de vue et des apports de chacun, ceci en prenant
       appui sur des relations interpersonnelles de qualité.
       Contrairement à des évolutions plus récentes comme l'holacratie,
       le modèle sociocratique est ouvert et libre.

       :source: https://fr.wikipedia.org/wiki/Sociocratie

       Voir: http://universite-du-nous.org/a-propos-udn/ses-outils/

   sobriété numérique
       La sobriété numérique consiste à prioriser l’allocation des ressources
       en fonction des usages, afin de se conformer aux limites planétaires,
      tout en préservant les apports sociétaux les plus précieux des
      technologies numériques.

      :source: https://theshiftproject.org/article/climat-insoutenable-usage-video/

   Soviet
       Conseil des travailleurs dans l'entreprise qu'ils occupent.
       Le premier est fondé par des anarchistes en 1905 à Saint Pétersbourg.

   SR
   Syndicalisme révolutionnaire

       Théorie et pratique du syndicalisme fondé sur la Charte d'Amiens


   Syndicat
   syndicats
   syndicat
       Structure de base de la C.N.T.

       Le rôle des syndicats interco et/ou interpro, essentiels dans un
       premier temps pour pouvoir grouper les syndiqué-e-s isolés
       dans leur secteur professionnel, doit être de favoriser et d'aider
       à la création de syndicats professionnels dans différents domaines en
       lien avec ses adhérents, tout en offrant un espace de solidarité et de
       **formation syndicale**.

       Le syndicat professionnel d'appartenance et ses assemblées générales
       restant l'**organe de base des décisions**.

       Le syndicat doit jouer un **rôle d'éducation populaire** permanent
       par **l'auto formation** et l'organisation régulières de débats, internes
       comme externes, la diffusion de nombreuses publications et l'édition de
       journaux, revues ou livres.

   techno-discernement
       accepter de réévaluer collectivement nos besoins pour adoter nos
       modes de consommation et de production.

       :source: "Technologie partout, démocratie nulle part", p. 215

   synallagmatique
   Synallagmatique
   synallagmatiques
       Lorsque les contractants s'obligent réciproquement les uns envers les autres.


   tribune
   Table du Congrès
       la table du congrès est composée du ou des président(e)s de séance, du
       ou des preneurs de tours de paroles, du ou des preneurs de notes.
       Elle est renouvellée à chaque demi-journée et elle est définie en
       ouverture de Congrès.

   ultracrépidarianisme
       Comportement qui consiste à donner son avis sur des sujets à propos
       desquels on n’a pas de compétence.

       :source: https://fr.wiktionary.org/wiki/ultracr%C3%A9pidarianisme

   Zen
       Il faut le rester dans tous les cas, et si ce n'est pas toujours évident
       c'est indispensable !
