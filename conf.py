#
# onestla.tech
#
import sys, os

import platform
from datetime import datetime

# https://docs.python.org/3.9/library/zoneinfo.html
from zoneinfo import ZoneInfo

source_suffix = ".rst"
master_doc = "index"
index_doc = "index"
now = datetime.now(tz=ZoneInfo("Europe/Paris"))
version = f"{now.year}-{now.month:02}-{now.day:02} {now.hour:02}H ({now.tzinfo})"
project = "onestla.tech"
copyright = f"2011-{now.year}, onestlatech, Creative Commons CC BY-NC-SA 3.0. Built with Python {platform.python_version()}"
release = version
exclude_patterns = [".venv", "build", ".git"]
extensions = ["sphinx.ext.intersphinx"]
html_theme = "bizstyle"
pygments_style = "colorful"
html_title = "Onestlatech"
html_short_title = html_title
intersphinx_mapping = {
    "http://france1.frama.io/juridique": None,
    "https://international.frama.io/cnt/": None,
}
extensions += ["sphinx.ext.todo"]
todo_include_todos = True
extensions += [
    # https://ablog.readthedocs.io/manual/markdown/
    "myst_nb",
]
# MyST config
myst_update_mathjax = False
myst_admonition_enable = True
myst_deflist_enable = True
extensions += [
    "ablog",
]
# https://ablog.readthedocs.io/manual/ablog-configuration-options/
#####################################################################
blog_path = "onestlatech"
# Base URL for the website, required for generating feeds.
blog_baseurl = "https://france1.frama.io/onestlatech/"
blog_title = "onestlatech"
# Post related
# Date display format (default is '%b %d, %Y') for published posts
post_date_format = "%Y-%m-%d"
# Number of seconds (default is 5) that a redirect page waits before
# refreshing the page to redirect to the post
post_redirect_refresh = 1
# Index of the image that will be displayed in the excerpt of the post.
# Default is 0, meaning no image.
# Setting this to 1 will include the first image, when available, to the excerpt.
# This option can be set on a per post basis using post directive option image
post_auto_image = 1
# Number of paragraphs (default is 1) that will be displayed as an excerpt from the post
post_auto_excerpt = 4
# Blog feeds
blog_feed_archives = True
blog_feed_fulltext = True
blog_feed_subtitle = False
blog_feed_titles = False
# Specify number of recent posts to include in feeds, default is None for all posts
blog_feed_length = None
# Font awesome
# ABlog templates will use of Font Awesome icons if one of the following is set: fontawesome_link_cdn
fontawesome_included = True
# https://ablog.readthedocs.io/manual/posting-and-listing/?highlight=blog_post_pattern#posting-with-page-front-matter
# Instead of adding blogpost: true to each page, you may also provide a
# pattern (or list of patterns) in your conf.py file using the blog_post_pattern option
blog_post_pattern = "news/*/*/*"
html_extra_path = ["feed.xml"]
liste_full = [
    "postcard.html",
    "recentposts.html",
    "sourcelink.html",
    "archives.html",
    "tagcloud.html",
    "categories.html",
    "searchbox.html",
]
html_sidebars = {
    "index": liste_full,
    "meta/**": liste_full,
    "chantiers/**": liste_full,
    "reunions/**": liste_full,
    "ethique/**": liste_full,
    "glossaire/**": liste_full,
    "manifeste/**": liste_full,
    "ressources/**": liste_full,
    "syndicats/**": liste_full,
}
extensions += [
    "sphinx_panels",
]
# Panels config
panels_add_bootstrap_css = False
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store", ".venv"]
pygments_style = "sphinx"
html_theme = "pydata_sphinx_theme"
html_theme_options = {
    "search_bar_text": "Search this site...",
}


