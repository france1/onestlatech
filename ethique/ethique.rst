.. index::
   ! Ethique

.. _ethique:

=====================================
Ethique
=====================================

.. toctree::
   :maxdepth: 3



TODO
=====

`Mupsi30/10/2020 <https://discordapp.com/channels/654441797539594277/771833419709022278/771835008834994226>`_

Je partage quelques ressources que je trouve pertinentes:

- DataGueule 42 "Des communs et des hommes" : https://peertube.datagueule.tv/videos/watch/7a961120-32e5-4b48-b71f-24d8f512cfc9
- Tobie Langel "Y a-t-il de la place pour l'éthique dans l'open source ?" : https://www.paris-web.fr/2020/conferences/is-there-room-for-ethics-in-open-source.php
- Marion Magné "Pourquoi les développeur·ses ont besoin d'une
  formation aux questions d'éthique : exemple de l'informatique de santé"
  https://www.paris-web.fr/2020/conferences/pourquoi-les-developpeureuses-ont-besoin-dune-formation-aux-questions-dethique-exemple-de-linformati.php
- Romuald Priol "“Fake” Green Tech : comprendre l'intérêt d'un Numérique
  Responsable" : https://www.paris-web.fr/2020/conferences/fake-green-tech-comprendre-linteret-dun-numerique-responsable.php
- Gille Dowek "L'émergence des questions éthiques en informatique" :
  https://joind.in/event/forum-php-2020/lemergence-des-questions-ethiques-en-informatique (vidéo à venir)

