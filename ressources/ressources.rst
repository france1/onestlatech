.. index::
   ! Ressources

.. _ressources_latech:

=====================================
Ressources
=====================================

.. toctree::
   :maxdepth: 4


   informatique/informatique
   sobriete_numerique/sobriete_numerique
   climat/climat
   economie/economie
   energie/energie
   politique/politique

