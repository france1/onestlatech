.. index::
   pair: Centrale énergie; Energie
   ! Centrale énergie

.. _centrale_energie:

=====================================
Centrale énergie
=====================================

.. seealso::

   - http://www.centrale-energie.fr/spip/index.php

.. contents::
   :depth: 3


Description
=============

Vous êtes sur le site du Groupe Professionnel Centrale-Energies qui réunit
des membres des associations d’anciens élèves des cinq Ecoles Centrales !

Le CO2 se déverse sans compter dans l’atmosphère et les énergies fossiles
sont de plus en plus chères...

Est-ce une crise de l’énergie ?

En tout cas, nous ne voulons pas vous laisser sans réponse.

Vous trouverez régulièrement ici des comptes rendus, des articles, des
idées, des liens...

Nos objectifs : informer Ingénieurs et Scientifiques, et plus généralement
la Société Civile, sur les évolutions de la donne énergétique, dans le
triple contexte mondial du réchauffement climatique, de la raréfaction
des ressources fossiles et de l’indépendance énergétique.

Six thèmes y sont étudiés sous l’angle technique, allant des sources
d’énergie à leur stockage, leur transfert et leur utilisation :

- énergies fossiles,
- nucléaire,
- renouvelables,
- vecteurs et stockage d’énergie,
- tous transports,
- bâtiments et urbanisme

Un septième thème transverse aux précédents les examine sous les
angles géopolitique, économique et environnemental

les matériaux de la transition énergétique : remplacer un problème par un autre ?
===================================================================================

.. seealso::

   - http://www.centrale-energie.fr/spip/IMG/pdf/presentation_centrale_energie\_-27-05-2020-e-hache.pdf
