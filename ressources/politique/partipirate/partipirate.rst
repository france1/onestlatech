.. index::
   pair: Parti pirate; Ressources

.. _partipirate:

=====================================
Parti pirate
=====================================

.. seealso::

   - https://wiki.partipirate.org/Accueil


.. toctree::
   :maxdepth: 3

   statuts/statuts
