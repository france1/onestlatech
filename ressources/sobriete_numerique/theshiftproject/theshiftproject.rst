.. index::
   ! theshiftproject

.. _theshiftproject:

============================================================================================================
theshiftproject (sobriété numérique)
============================================================================================================

.. seealso::

   - https://theshiftproject.org/
   - https://twitter.com/theShiftPR0JECT
   - https://twitter.com/JMJancovici
   - https://twitter.com/Les_shifters



.. contents::
   :depth: 3


Description
==============================

Le Shift est géré au quotidien par une équipe salariée d’une dizaine de
personnes dont Matthieu Auzanneau est le directeur.

Le président Jean-Marc Jancovici et le Bureau supervisent les activités
tandis que des chefs de projet pilotent les groupes de travail.

Le think tank bénéficie du soutien de bénévoles qui ont constitué une
association autonome : Les Shifters.

Méthode SMERT
================

.. seealso::

   - :ref:`smert`


Sobriété numérique pour les décideurs
=========================================

.. toctree::
   :maxdepth: 3

   sobriete_numerique/sobriete_numerique


Plan de transformation de l’économie française (PTEF)
=========================================================

.. toctree::
   :maxdepth: 3

   ptef/ptef


Rapports
=========

.. toctree::
   :maxdepth: 3

   rapports/rapports
