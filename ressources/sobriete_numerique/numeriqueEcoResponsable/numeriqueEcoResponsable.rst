.. index::
   pair: numeriqueEcoResponsable; Ressources
   pair: Richard Hanna ; numeriqueEcoResponsable

.. _numeriqueEcoResponsable:

============================================
numeriqueEcoResponsable par Richard Hanna
============================================

.. seealso::

   - https://github.com/supertanuki/numeriqueEcoResponsable/
   - https://github.com/supertanuki/numeriqueEcoResponsable/graphs/contributors

.. contents::
   :depth: 3


TODO
=====

- https://discordapp.com/channels/654441797539594277/772803431115128832/772913703624507412
- https://www.arcep.fr/la-regulation/grands-dossiers-thematiques-transverses/lempreinte-environnementale-des-reseaux.html
- https://discordapp.com/channels/654441797539594277/772803431115128832/772910361783304212


https://github.com/supertanuki/numeriqueEcoResponsable/
----------------------------------------------------------

- https://twitter.com/richardhanna
- https://richardhanna.dev/
- https://github.com/supertanuki
