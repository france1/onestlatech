.. index::
   ! lescommuns

.. _lescommuns:

==================
lescommuns
==================

.. seealso::

   - https://lescommuns.org/


.. contents::
   :depth: 3



Qu’est-ce qu’un bien Commun ?
==============================

Les biens communs, ou tout simplement communs, sont des ressources,
gérées collectivement par une communauté, celle-ci établit des règles
et une gouvernance dans le but de préserver et pérenniser cette ressource.

Des logiciels libres aux jardins partagés, de la cartographie à l’énergie
renouvelable, en passant par les connaissances et les sciences ouvertes
ou les AMAPs et les épiceries coopératives, les « Communs » sont partout !

En d’autres termes on peut définir les communs comme une ressource
(bien commun) plus plus les interactions sociales (économiques, culturelles
et politiques) au sein de la communauté prenant soin de cette ressource.

On peut aussi définir les biens communs comme la recherche par une
communauté d’un moyen de résoudre un problème en agissant au bénéfice
de l’ensemble de ses membres.

Il est important de noter que la définition des communs est un chantier
à part entière toujours en cours, à l’image de leur diversité.

La définition ci-dessus est proposée dans un but de vulgarisation de
la notion de communs.
