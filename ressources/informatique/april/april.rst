.. index::
   ! APRIL

.. _april:

===========================================================
APRIL : Promouvoir et défendre le logiciel libre
===========================================================

.. seealso::

   - https://april.org/


.. contents::
   :depth: 3


Description
==============================

Depuis 1996, l'April est animée par une ambition : « logiciel libre, société libre ».

Pionnière du logiciel libre en France, l'April, constituée de 3972 adhérents
(3666 personnes physiques, 306 entreprises, associations et organisations),
est depuis 1996 un acteur majeur de la démocratisation et de la diffusion
du logiciel libre et des standards ouverts auprès du grand public,
des professionnels et des institutions dans l'espace francophone.
