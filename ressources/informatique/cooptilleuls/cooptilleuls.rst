.. index::
   ! les-tilleuls

.. _coopTilleuls:

==================
coopTilleuls
==================

.. seealso::

   - https://twitter.com/coopTilleuls
   - https://les-tilleuls.coop/fr


.. contents::
   :depth: 3


Description
=============

Les-Tilleuls.coop, c'est une société d'origine lilloise avec une forte
expertise API, Symfony, JavaScript et DevOps appliquée au e-commerce
et aux applications web innovantes.

Notre équipe de consultants vous accompagne dans la réussite de vos projets :
formation, choix technologiques, architecture logicielle, direction
technique, urbanisation, industrialisation, audit de qualité, de performance
et de sécurité.

Notre équipe de développeurs réalise vos plates-formes e-commerce ainsi
que vos applications web et mobiles au plus proche de l'état de l'art,
en suivant les bonnes pratiques de l'industrie.

