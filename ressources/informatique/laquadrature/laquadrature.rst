.. index::
   ! laquadrature

.. _laquadrature:

============================================================================================================
laquadrature
============================================================================================================

.. seealso::

   - https://www.laquadrature.net/


.. contents::
   :depth: 3


Description
==============================

La Quadrature du Net promeut et défend les libertés fondamentales dans
l’environnement numérique.

L’association lutte contre la censure et la surveillance, que celles-ci
viennent des États ou des entreprises privées.

Elle questionne la façon dont le numérique et la société s’influencent
mutuellement.

Elle œuvre pour un Internet libre, décentralisé et émancipateur
