.. index::
   ! reset.fing

.. _restet_fing:

==================
reset.fing
==================

.. seealso::

   - https://reset.fing.org/

.. contents::
   :depth: 3

Reset 2022 : transformer le numérique
==========================================

Après un diagnostic sévère sur le numérique d’aujourd’hui et une
proposition de **qualités** à développer (Reset 2018-2019), le
programme #RESET 2022 propose d’entreprendre des actions concrètes
et transformatrices à horizon court-moyen terme (2022) avec les
concepteurs, les commanditaires et les usagers du numérique.

Un programme à vocation d’impacts
====================================

Impacts recherchés : éthiques, sociaux, environnementaux, concurrentiels,
de gouvernance et régulation.

- Des critères actionnables pour le soutien à l’innovation, pour la maîtrise d’ouvrage,
- des perspectives grand’ouvertes pour la conception responsable,
- des facteurs d’attractivité pour les employeurs du numérique,
- des conditions culturelles et éducatives pour renforcer notre maîtrise des choix numériques…


Dispositif
============

.. toctree::
   :maxdepth: 3

   dispositif/dispositif
