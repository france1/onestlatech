.. index::
   pair: Dispositif; RESET

.. _dispositif_reset:

==========================
Le dispositif #RESET
==========================

.. seealso::

   - https://reset.fing.org/participer-a-reset/le-dispositif.html

.. contents::
   :depth: 3


Introduction
==============

RESET entend créer les conditions de la transformation d’un numérique
que nous subissons vers un numérique que nous voulons, en travaillant
simultanément sur :

- ce que l’on attend du numérique ;
- ce que des coalitions d’acteurs peuvent faire pour retrouver la maîtrise de leurs choix ;
- des actions communes, à l’échelle locale, nationale et européenne.

Cette page vous présente de manière résumée la vision et la méthodologie
élaborées dans le Cahier d’enjeux Reset, publié en 2019.

.. figure:: qualites-numériques.webp
   :align: center
