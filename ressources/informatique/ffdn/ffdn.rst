.. index::
   ! FFDN

.. _ffdn:

============================================================================================================
FFDN Fédération des Fournisseurs d'Accès Internet Associatifs
============================================================================================================

.. seealso::

   - https://www.ffdn.org/


.. contents::
   :depth: 3


Description
==============================

La fédération FDN regroupe des Fournisseurs d'Accès à Internet associatifs
se reconnaissant dans des valeurs communes : bénévolat, solidarité,
fonctionnement démocratique et à but non lucratif ; défense et promotion
de la neutralité du Net.

À ce titre, la fédération FDN se donne comme mission de porter la voix
de ses membres dans les débats concernant la liberté d'expression et la
neutralité du Net.

Elle fournit à ses membres les outils pour se développer et répondre aux
problématiques qui concernent l'activité de fournisseur d'accès à Internet.



