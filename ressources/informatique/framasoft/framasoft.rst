.. index::
   ! Framasoft

.. _framasoft:

===========================================================
Framasoft : Changer le monde, un octet à la fois
===========================================================

.. seealso::

   - https://framasoft.org


.. contents::
   :depth: 3


Description
==============================

Framasoft, c’est une association d’éducation populaire, un groupe d’ami·es
convaincu·es qu’un monde numérique émancipateur est possible, persuadé·es
qu’il adviendra grâce à des actions concrètes sur le terrain et en ligne
avec vous et pour vous !


