.. index::
   ! codefor

.. _codefor:

====================================================================
codefor.fr Mettre la technologie au service de l'intérêt général
====================================================================

.. seealso::

   - https://codefor.fr/


.. contents::
   :depth: 3


Description
==============================

Nous promouvons des idées et développons des outils au service d'un monde
numérique libre et ouvert offrant des opportunités à tou·te·s les citoyen·ne·s.

Nos thématiques : Participation, Transparence & Communs numériques
