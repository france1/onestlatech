.. post:: 2020-10-15
   :tags: SMERT, sobriété numérique, vidéo
   :category: Méthodes
   :author: TheShiftProject
   :location: France
   :language: fr

.. index::
   pair: Sobriété numérique ; 2020

.. _shift_project_smert_2020_10_15:

=================================================================================================
**Déployer la sobriété numérique** par TheShiftProject
=================================================================================================

.. seealso::

   - https://theshiftproject.org/article/deployer-la-sobriete-numerique-rapport-shift/
   - https://www.ladn.eu/tech-a-suivre/smart-tech-impact-rapport-shift-project/
   - :ref:`theshiftproject_sobriete`

.. contents::
   :depth: 3


Vidéo
=======

.. seealso::

   - https://www.youtube.com/watch?v=ZIVfMHL7ALk


The Shift Project a présenté le 15 octobre 2020 son nouveau rapport sur
l'impact environnemental du numérique : « Déployer la sobriété numérique ».

👉 Introduction par Jean-Marc Jancovici (Président, The Shift Project)
   et Raphaël Guastavi (Ademe)

👉 Présentation du rapport par :

- Hugues Ferreboeuf, directeur du groupe de travail, The Shift Project
- Maxime Efoui, coordinateur du projet, The Shift Project
- Laurie Marrauld, co-pilote du groupe de travail, The Shift Project
- Céline Lescop, co-pilote du groupe de travail, The Shift Project

👉 Réactions de :

- Giovanni D'Aniello, Group CTO, AXA
- Agnès Catoire, Membre de la Convention Citoyenne pour le climat
- Henri d'Agrain, Délégué Général, CIGREF
- Arnaud Gueguen, Consultant et formateur, membre du groupe de travail

👉 Retrouvez le rapport en ligne: https://theshiftproject.org/article/deployer-la-sobriete-numerique-rapport-shift/

👉 Les supports de présentation sont disponibles ici: https://theshiftproject.org/article/deployer-la-sobriete-numerique-rapport-shift/

👉 Pour recevoir des nouvelles du Shift Project : https://forms.gle/Bd9eHM9GknTnTf428 »



Introduction
============

Dans un nouveau rapport publié jeudi 15 octobre 2020, le think tank The
:ref:`Shift Project <theshiftproject>` propose une méthodologie pour
aller vers une sobriété numérique.

Et cela implique d'évaluer le réel bénéfice des smart cities et autres
lampes intelligentes.

On commence à le savoir.

**Le numérique n’a rien d’immatériel et son impact sur l’environnement
est loin d’être négligeable**.

Sa production et son utilisation représentent aujourd’hui 4% des émissions
carbonées mondiales et pourraient atteindre 8% en 2025 si rien n’est fait,
selon The Shift Project.

Dans ses deux précédents rapports, le think tank alertait, chiffres
à l’appui, sur les conséquences environnementales de nos usages numériques.

Son troisième volet **Déployer la sobriété numérique** s’intéresse à la
méthodologie à suivre pour réduire cet impact.

Il s’adresse avant tout aux entreprises et aux collectivités.

Tous les outils numériques ne font pas partie de la solution
===============================================================

Le rapport invite notamment à déconstruire certaines expressions comme
«smart», «green» ...

Elles vendent l’idée qu’une technologie, voire une ville entière
(les fameuses smart cities), est capable de réduire notre consommation
énergétique.

« Le problème c’est qu’il n’y a pas de contenus normatifs derrière »,
pointait Jean-Marc Jancovici, président de l'association, lors d’une
conférence de presse jeudi 15 octobre.

Certaines technologies ont effectivement un potentiel de gain environnemental,
mais « il faut arrêter de percevoir tous les outils numériques comme
des solutions par défaut », estime Maxime Efoui-Hess, l’un des auteurs du rapport.

Pour trancher sur le bénéfice réel d’une technologie pour l’environnement,
The Shift Project propose une méthode d’évaluation baptisée **SMERT**
(pour Smart Technologies Energy Relevance Model).

Elle consiste à faire un bilan complet en mettant dans la balance, d’un
côté les opportunités d’économies sur la consommation énergétique permises
par la technologie, et de l’autre l’énergie consommée pour la mettre
en place.

Le tout, en prenant en considération l’ensemble du cycle de vie de la
technologie.


