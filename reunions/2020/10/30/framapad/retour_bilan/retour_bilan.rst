.. index::
   pair: Bilan ; 2020-10-30
   ! Liste canadienne


.. _retour_bilan_2020_10_30:

=====================================================================
1. Retour et bilan de l'appel pour une autre réforme des retraites
=====================================================================

.. seealso::

   - https://mensuel.framapad.org/p/onestlatech-9jt6?lang=fr
   - https://onestla.tech/publications/une-autre-tech-est-possible/


.. contents::
   :depth: 3

Kevin: est-ce que c'est un mouvement de gauche ?
==================================================

La gauche de la tech française était peu structurée et peu visible dans
notre corps de métier, depuis au moins l'élection, alors qu'il y a dans
la communauté des gens intéressées sur les enjeux de la tech et le modèle
de société associé.

Mais les retraites n'était qu'un sujet de départ, l'épidémie de COVID/confinement
a accéléré le processus , et la technologie et les personnes qui la
mettent en oeuvre ont un rôle à jouer, sur différents enjeux (sociaux,
environnementaux)

Volonté pour pas mal de personnes de tenir des contre-discours pour
pousser la technologie dans une autre direction, proposer un autre modèle
de société pour proposer un modèle qui serait plus social, plus égalitaire,
plus démocratique.

Dès le départ on s'est dit qu'on mettait tout en œuvre pour faire retirer
la réforme des retraites.

Le COVID a perturbé ce "combat", la problématique a changé (lien avec
d'autres combats, la famille Traoré etc.)

L'idée est préparer la suite, tour de parole  :

Question : est-ce que c'est un mouvement de gauche ?

Hélène : utilisation du vocabulaire : multipolitique, apartisan, etc...
=========================================================================

pas forcément rattaché à un mouvement/parti particulier, ni de se rattacher
à une personnalité particulière de gauche, "crainte" de voir des individus
changer de bord, risque de la récupération politique [déplacer tout ça en 2. ?]

[n'hésitez pas à prendre des notes aussi, on fusionnera après !]

Question : remarque de vocabulaire là-dessus, les gens qui se disent
apolitiques se révèlent souvent être d'extrême-droite.

Proposition de se dire "apartisan" à la place pour ne pas se rattacher
à une mouvance politique particulière.

Ce mouvement s'est quand même constitué contre la réforme des retraites
[et est socialiste dans un sens ?].

Proposition d'utiliser le terme "multipolitique" pour être plus inclusif ;
"apartisan" sous-entendrait de se mettre en marge des partis politiques
préexistants alors que "multipolitique" accepte que le mouvement soit
rattaché à plusieurs partis qui existent déjà.

Être accessible comme l'a fait Nicolas Hulot dans la précédente campagne
en 2017. Ouverts à discussion et pas adossé  un parti.

Communs au lieu de Communistes, Capital au lieu de Capitalistes...
Être proactif dans le discours.

Une idée: "coop-politique", un groupe utilisant la technologie et des
systèmes de gouvernance horizontales pour prendre des positions/directions
sur chaque sujets.

Soucis avec la notion de "politique", etc. : la question commence par
"qu'est-ce qu'on veut faire", association/syndicat/parti.

Comprend bien le but de ne pas être récupéré par un parti existant ou
autre, peut-être que c'est un peu tôt de se poser ces questions, mais
quel est le but de ce mouvement au-delà de la réforme des retraites ?

Kevin : se demander dans quel sens on veut aller
==================================================

ça ne sert à rien de créer un parti ou un syndicat, le but est de se
regrouper autour de valeurs qui [] même si c'est pas dans une organisation
type parti/syndicat, ça reste de la politique ; dans le texte initial
il y avait quand même des valeurs dominantes de société : répartir de
manière égalitaire les richesses dans la société (1% vs le reste, projet
anticapitaliste) ; questions (plutôt portées par la gauche) de féminisme,
de diversité, de lutte contre le racisme ; comment on utilise la technologie
pour porter ces idées là et comment à l'inverse on s'oppose à la
technologie quand elle va  dans le sens contraire ; les gilets jaunes et
comment on se rattache à une idée de contrôle du pouvoir, de la démocratie
et comment on fait écho aux questions démocratiques sur ce sujet.

À mon avis, plutôt que de chercher une étiquette on doit se retrouver
autour de ces valeurs-là tout en autorisant le rattachement à d'autres
structures (exemple du parti pirate).

Regrouper tous les acteurs de la technologie et se demander dans quel
sens on veut aller sans pour autant se lancer dans des discussions
théoriques ni se priver de regrouper des gens qui sont déjà organisés,
dans des partis, des syndicats, etc.

Mouvement de gauche dans le sens où l'organisation est horizontale, tout
le monde a le droit de prendre la parole.

Projet de société (à redéfinir)
------------------------------------

1. Juste répartition des richesses
2. Diversité (féminisme, LGBTQI+, antiracisme, etc.)
3. Le rapport de la société et de son pouvoir à la technologie

[Rien noté de l'intervention du M. de CNT]

(plus sûr) d'expérience c'est plus efficace de lutter ensemble sur un
objectif commun clair plutot que de se prendre le chou sur une idéologie

Ce qui nous différencie nous comme groupe d'un syndicat (Sud Solidaires, CNT…)
quelle est la différence entre onestla.tech et un syndicat dans l'informatique ?

On n'a pas envie d'être une "corporation" ; si on veut porter un projet
de société.

But
----

avoir comme projet d'écrire un texte qui définirait nos valeurs.

Quand on écrit nos valeurs, ça nous force à nous définir [par rapport au
fait de se qualifier comme "de gauche"].

Concrètement qu'est-ce qu'on fait ?

.. _idee_manfeste_2020_10_30:

@Diether : importance du texte fondateur pour structurer un mouvement
=========================================================================

.. seealso:: :ref:`chantier3_2020_10_30`

:ref:`importance du texte fondateur pour structurer un mouvement <chantier3_2020_10_30>`.

Sur les valeurs et sur comment on va s'appeler, vers quoi on porte, il
faut déjà écrire le manifeste ; personne ne choisit comment il est perçu
de l'extérieur. Ce qu'il faut décider c'est ce qu'on souhaite porter comme valeurs

Sur le chat :

- Pour rouvrir la parenthese sur le nom, #onestlatech  me semble exclure
  de facto l'usager finale et donc la "société civile".
- idée de manifeste (pour les thématiques éthiques) ? https://reset.fing.org/participer-a-reset/le-dispositif.html


klaus: atteindre tous les acteurs de la tech, y compris les associations et les syndicats
==========================================================================================

[ranger dans :ref:`5. ? <liens_2020_10_30>`]

d'accord sur l'idée du manifeste, pour construire la structure il est
nécessaire d'écrire un manifeste quitte à partir du premier manifeste
contre la réforme des retraites.

Concernant les syndicats le but n'est pas de rejoindre les objectifs
politiques d'un syndicat (comparaison avec Sud ou CNT), le but est de
défendre les travailleurs, initialement au sujet de la réforme des
retraites et de **porter une vision de société basée sur la technologie**.

Beaucoup de discussions sur les outils, le web décentralisé, amener des
réflexions et faire prendre conscience de certains sujets sociaux, ce
qu'un syndicat ne fait pas (exemple d'Antonio Casili sur le travail
dissimulé).

But
----

Organiser de façon plus large que les syndicats et **atteindre tous
les acteurs de la tech, y compris les associations et les syndicats**

@josef : Tyrannie de l'absence de structure
============================================

[ranger dans :ref:`4. ? <campagnes_2020_10_30>`]

en plus d'un manifeste qui nous aiderait à clarifier nos positions
idélogiques et de travaux qui permettrait de savoir quelle société
on aimerait avoir, quelles sont les actions concrètes qui nous
permettraient de porter ce projet ?

Est-ce que ça devrait pas décider de la structure qu'on devrait avoir
pour mener nos actions ?

Exemple de la 5G dans lequel on devrait avoir un support d'information,
exemple de l'illectronisme, proposer des formations en ce sens.

[raté une intervention]

La Tyrannie de l'absence de structure (https://infokiosques.net/lire.php?id_article=2) :
clarifier le manifeste, par petits groupes on arrive à construire des choses

@kay: j'attends du soutien
==============================

qu'est-ce que j'attends du collectif ? Une fois que j'ai compris ce que
sont les implications politiques de mon métier, si mon patron me demande
de faire un truc et que je dis non, j'attends du soutien

@celian : qu'est-ce qu'une liste canadienne ?
================================================

qu'est-ce qu'une liste canadienne ?

avid
=======

sert à aider à la prise de parole des femmes ou des gens qui parlent peu,
réparti selon le genre [ou autre] et ensuite selon la prise de parole

@ebo : écriture du manifeste
========================================

écriture du manifeste, premier groupe qui propose de l'écrire sur git.

Si on passe trop de temps à se demander qui on est on va se faire des
nœuds au cerveau et ne pas avancer sur la suite.

Première mobilisation contre la loi travail. Comment on mobilise ?
Site, mailing-list, blog, est-ce que le discord ça suffit ?

"Réagir en cas d'attaque". Est-ce qu'on crée un sous-groupe
"éducation populaire" ou est-ce que c'est trop chiant ?

Groupe thématique ? Propositions sur la 5G, "onestla.tech pense que", etc.

@olaf : rédaction du manifeste
=================================

rédaction du manifeste : les petits groupes sont un problème en
particulier en absence d'organisation politique et problème des
violences dont on n'a pas forcément conscience.


.. _naywel_outils_2020_10_30:

@fritz: faire un groupe de travail sur les outils
====================================================

est-ce qu'un discord suffit ? Pas très à l'aise avec Discord, question
de confidentialité des données et logiciel libre, **cohérence entre les
moyens d'action et ce qu'on défend**.

Possibilités : Mumble, Jabber, Jitsi-meet etc.

Faire un :ref:`groupe de travail sur les outils <chantier2_2020_10_30>`.

@samson: associer les usagers "finaux" aux développements
============================================================

associer les usagers "finaux" aux développements, exemple de l'e-learning.
À quel moment quand on écrit nos tests ou nos US on n'a pas une association
d'usager pour venir donner son avis.

Exemple de StopCovid/TousAntiCovid, est-ce qu'on ne fait pas du numérique
partout quand on devrait faire du social à la place ?

Au niveau du collectif, trouver le plus petit dénominateur commun et
"peer"-er ensuite

@ralf: privilégier au maximum le libre
=====================================================

le libre c'est un sujet qui me tient à cœur et qui pourrait servir à
contrer les grandes organisations capitalistes (comme Framasoft le fait)
et Onestla.tech pourrait permettre ça.

L'idée qu'Onestla.tech pourrait certifier **qu'on souhaite privilégier
au maximum le libre**.

L'idée qu'on ne peut pas tout faire avec le libre vient du fait qu'on
est limité en termes de contribution, et le mouvement pourrait porter
le message contraire à un niveau national/européen/mondial.

Contrer le "big tech" par les "petites techs"

@robert : respect des utilisateurs et de l'inclusion
===========================================================

quels moyens opérationnels pour porter notre projet de société ?

Qu'est-ce qu'on est prêts à faire ? Pousser la low-tech, des nouvelles
technologies plus inclusives, plus respectueuse de l'environnement…

C'est pour ça que ce collectif porte ce nom-là, on va pas s'appeler
"Onestla.tech" parce qu'on cherche à inclure les dockers marseillais.

C'est la première fois qu'on entend parler de gauche et de projet
alter/anti-capitaliste [dans la tech] et je pense que c'est important
de dire que nous, on est la tech de la qualité, du respect des
utilisateurs et de l'inclusion.

Exemple de la loi El Kohmri et des messages type "quand on veut bloquer
l'économie on bloque pas une université" : aujourd'hui les "lieux de pouvoir"
ont bougé et on est capable de dire à nos patrons que si on ne bosse pas
il ne se passe rien.

@gregor: comment travailler en dehors du capitalisme ?
==========================================================

"on ne veut pas remplacer les caissières par des écrans" : si, est-ce
que le problème c'est de rempalcer un travail ignoble par un écran qui
le fait automatiquement ?

Le problème c'est pas ça c'est que la caissière qui ne profite pas de
ce remplacement.
Si elle pouvait se retrouver chez elle à faire des études ou autres,
est-ce que ça ne serait pas mieux ?
On est aussi là pour montrer que la technologie sert aussi, quand elle
est utilisée à bon escient, peut aussi profiter au plus grand nombre.

comment travailler en dehors du capitalisme ?

Inculture 5: Travailler moins pour gagner plus Conférence gesticulée de
Franck LEPAGE et Gaël TANGUY https://invidious.fdn.fr/watch?v=X5BAozcoY8s

problématique : en tirer un moyen de subsistance
