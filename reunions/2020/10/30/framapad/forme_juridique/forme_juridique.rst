.. index::
   pair: Forme juridique ; 2020-10-30
   pair: Forme juridique ; Fédération

.. _forme_juridique_2020_10_30:

======================================================================================
3. Forme juridique du collectif (association loi de 1901 ou association de fait ?)
======================================================================================

.. seealso::

   - https://mensuel.framapad.org/p/onestlatech-9jt6?lang=fr
   - https://onestla.tech/publications/une-autre-tech-est-possible/


.. contents::
   :depth: 3


@joans : association de défense des droits pour les lanceurs d'alerte ou autres
==================================================================================

aller jusqu'à une association de défense des droits pour les lanceurs
d'alerte ou autres.

Pas de concurrence aux syndicats, mais proposer un collectif pour aider
les indépendants ou les sceptiques (des syndicats) à se retrouver dans
un groupe qui peut les accompagner.

@arno: une association est protéiforme
=============================================

une association est protéiforme, si elle doit devenir un syndicat/un parti/autre
chose elle le deviendra ; l'association 1901 c'est la forme la plus neutre
et laïque quand on ne sait pas vraiment vers où on va et qu'on veut
juste se réunir et avoir un statut.

En écho au forum et sur les moyens d'action : le but est que tous les
CFP (call for papers) ou conférences qu'on voit passer puissent être
l'objet d'une intervention du collectif, que ça soit sur l'éthique,
sur la visibilité des femmes/minorités, sur le droit du travail

@ambros
--------

pourquoi faire une association ?
Pas forcément utile si on veut seulement discuter impression de recréer
un syndicat (mais pas contre)

fonctionnement en utilisant le mandat impératif

Arguments pro associatifs
============================

@merlin
--------

qui a la main sur les outils ?
Qui est garant des status déposés ?

Besoin pour un compte bancaire, pour financer serveur (auto hébergement)
Qui porte la parole du collectif ? (contrôler son image)

Une association peut se pourvoir en justice pour un de ses membres
Exemple du harcélement au travail, asso peut défendre un individu

Arguments contre
===================

@norbert
-----------

- se faire dicter fonctionnement par état
- personnes responsables juridiquement des agissements de tous
- se fermer aux autonomes (qui veulent pas rentrer dans les assos)
- possible de se faire une image publique forte malgré pas structure
- avoir un compte + représentation publique possible sans asso
- thunes : possible de demander à d'autres organisaitons (ffdn, chatons),
  même si pas idéal
- fédération autogérée : bon outil pour commencer quitte à évoluer ensuite
