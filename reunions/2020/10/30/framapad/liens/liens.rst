.. index::
   pair: Liens ; 2020-10-30

.. _liens_2020_10_30:

================================================================================================
5. Liens avec les autres structures (syndicats, organisations nationales et internationales)
================================================================================================

.. seealso::

   - https://mensuel.framapad.org/p/onestlatech-9jt6?lang=fr
   - https://onestla.tech/publications/une-autre-tech-est-possible/


.. contents::
   :depth: 3


Quelques orgas proche ou pas
=============================

.. seealso::

   - :ref:`ressources_latech`

- https://lescommuns.org/
- https://codefor.fr/
- https://april.org
- https://framasoft.org
- https://chatons.org/
- https://www.ffdn.org/
- https://www.laquadrature.net/
- https://adullact.org/

@nborri
=========

intérêt de collaborer / échanger avec les autres structures qui se posent
des questions très similaires et partagent les mêmes valeurs "de gauche" / sociales...

Notamment : le collectif Designers Ethiques https://designersethiques.org/
(qui travaille sur les questions "éthiques" que posent les services numériques
et le design plus largement, composé essentiellement de designers...mais
pas que !) et le Mouton Numérique https://mouton-numerique.org/
(collectif techno-critique, pas "anti-tech")

https://www.techtrash.fr/ (humour technocritique)

Le monde du logiciel libre en général
Les syndicats

