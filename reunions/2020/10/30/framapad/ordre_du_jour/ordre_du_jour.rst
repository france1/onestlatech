.. index::
   pair: Ordre du Jour ; 2020-10-30

.. _odj_2020_10_30:

=========================================================================
ODJ : https://onestla.tech/publications/une-autre-tech-est-possible/
=========================================================================

.. seealso::

   - https://onestla.tech/publications/une-autre-tech-est-possible/
   - https://mensuel.framapad.org/p/onestlatech-9jt6?lang=fr
   - https://onestla.tech/publications/une-autre-tech-est-possible/

.. contents::
   :depth: 3


Ordre du jour
===============

- :ref:`Retour et bilan de l'appel onestla.tech <retour_bilan_2020_10_30>`
  pour une autre réforme des retraites
  (cf la mention dans le livre Technologie partout, démocratie nulle part
  et ainsi que l'article Se reconnaître et lutter comme travailleur·ses du
  numérique dans la revue Mouvements
- :ref:`Établissement du fonctionnement du collectif <fonctionnement_2020_10_30>`
  (en partant de cette proposition comme base de travail)
- :ref:`Forme juridique du collectif <forme_juridique_2020_10_30>`
  (association loi de 1901 ou association de fait ?)
- :ref:`Thématiques et campagnes à mener <campagnes_2020_10_30>`
- :ref:`Liens avec les autres structures (syndicats, organisations nationales et internationales) <liens_2020_10_30>`

