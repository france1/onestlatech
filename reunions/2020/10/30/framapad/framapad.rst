.. post:: 2020-10-30
   :tags: 2020-10-30, réunion constitutive
   :category: Réunion
   :author: onestla.tech
   :location: France
   :language: fr

.. index::
   pair: Framapad ; 2020-10-30

.. _framapad_2020_10_30:

================================================================================================
Vendredi 30 octobre 2020 compte rendu Réunion onestla.tech constitutive sur Framapad
================================================================================================

.. seealso::

   - https://mensuel.framapad.org/p/onestlatech-9jt6?lang=fr
   - https://onestla.tech/publications/une-autre-tech-est-possible/


.. toctree::
   :maxdepth: 3

   ordre_du_jour/ordre_du_jour
   retour_bilan/retour_bilan
   fonctionnement/fonctionnement
   forme_juridique/forme_juridique
   campagnes/campagnes
   liens/liens


Suite
======

- Prochaine réunion : :ref:`vendredi 6 novembre 2020 19h <reunion_2020_11_06>`
- se répartir sur les thématiques qui nous intéressent
