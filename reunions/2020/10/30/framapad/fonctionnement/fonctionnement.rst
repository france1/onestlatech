.. index::
   pair: Fonctionnement ; 2020-10-30

.. _fonctionnement_2020_10_30:

===========================================================================================================
2. Établissement du fonctionnement du collectif (en partant de cette proposition comme base de travail)
===========================================================================================================

.. seealso::

   - https://mensuel.framapad.org/p/onestlatech-9jt6?lang=fr
   - https://github.com/loomio/loomio

.. contents::
   :depth: 3


@anton
=========

retour sur l'ordre du jour point 2. : propose l'outil Loomio qui est un
outil de prise de décision sasn qu'on soit tous en réunion forcément en
même temps (repo github : https://github.com/loomio/loomio )

Proposition d'une organisation asynchrone (type loomio ?):

- prise de décision par sondage
- si désaccord : créer groupe de travail entre personnes prêtes à y
  réfléchir, ce groupe propose un sondage pour résoudre
