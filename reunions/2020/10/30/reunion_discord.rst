.. index::
   pair: Discord ; 2020-10-30

.. _discord_2020_10_30:

==========================================================================================================
Vendredi 30 octobre 2020 réunion Parce-qu'une autre tech est possible, organisons-nous ! sur discord
==========================================================================================================

.. seealso::

   - https://onestla.tech/publications/une-autre-tech-est-possible/


.. contents::
   :depth: 3


Ordre du jour
===============

.. seealso::

   - https://onestla.tech/publications/une-autre-tech-est-possible/

Retranscription
===================

.. toctree::
   :maxdepth: 3


   framapad/framapad
