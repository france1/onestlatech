.. post:: 2020-11-06
   :tags: réunion, discord
   :category: Réunion
   :author: onestla.tech
   :location: France
   :language: fr

.. _reunion_2020_11_06:

====================================================================
Compte rendu de la réunion onestla.tech du vendredi 6 novembre 2020
====================================================================


.. seealso::

   - https://github.com/onestlatech/onestlatech.github.io/pull/1110
   - https://annuel2.framapad.org/p/2876lgm0mw-9jxu?lang=en


.. contents::
   :depth: 3

Réunion précédente
===================

.. seealso::

   - :ref:`framapad_2020_10_30`

CR de la réunion précédente: https://mensuel.framapad.org/p/onestlatech-9jt6?lang=fr


.. _decisions_2020_11_06:

Décisions prises pendant cette réunion
=========================================

.. seealso::

   - :ref:`loomio`


La team infra a choisi :ref:`Loomio <loomio>` pour l'organisation des votes,
confirmé par un vote à main levée de l'AG pour pouvoir avancer sur les
autres sujets

Le rythme des réunion va changer: une semaine sur deux le soir, et
l'autre semaine le midi, pour pouvoir arranger un maximum de monde.

La prochaine réunion aura lieu Mardi 17 Novembre 2020 à 12h30 (pour 1h~1h15 max)

Nous ne voulons pas créer un label **onestlatech** pour noter les entreprises.


Objectifs pour la réunion suivante
=======================================

:Vote: onestla.tech: une association de fait, une association loi 1901,
    ou un syndicat ?

    Ce document a été rédigé pour vous aider à prendre votre décision:
    https://hebdo.framapad.org/p/R%C3%A9capitulatif_des_diff%C3%A9rences_structurelles-9jv3?lang=fr

:Vote: Acceptons-nous que le collectif fonctionne suivant l'organisation
    décrite dans ce document ?  https://github.com/onestlatech/onestlatech.github.io/pull/1110

:Vote/Elections: Qui sont les mandataires du collectif, pour les rôles suivants:

    - Gestion des réseaux sociaux
    - Réponse aux mails
    - Modération du serveur Discord
    - Administrateur(s) Loomio
    - Administrateur(s) GitHub
    - Référent pour chaque chantier


:Rédaction: Premier jet d'un manifeste / code de conduite à vocation interne

:Atelier: Formation à la **sociocratie** par Mehdi Guiraud, lundi 9 novembre de 12H30 à 14H
    Inscription: https://pad.mehdiguiraud.info/p/OnEstLaTech_sociocratie


Compte Rendu
=================

.. seealso::

   - :ref:`chantiers_latech`

Combien de temps donné à cette réunion ?

Une heure, de 19h15 à 20h15 (elle a terminé à 20h45)

Pour rappel, un découpage par :ref:`chantier <chantiers_latech>` a été fait
sur le salon discord, beaucoup de ressources y sont disponibles.

Gouvernance et structure interne
=====================================

.. seealso::

   - :ref:`chantier_1_latech`
   - https://hebdo.framapad.org/p/R%C3%A9capitulatif_des_diff%C3%A9rences_structurelles-9jv3?lang=fr

:Jul: Nous avons rédigé un document qui décrit les différents types de
    structures que peut prendre notre collectif.

    - https://hebdo.framapad.org/p/R%C3%A9capitulatif_des_diff%C3%A9rences_structurelles-9jv3?lang=fr
      (ouvert à modification pour toute personne qui veut ajouter des infos)

    On a des gens intéressés par une action syndicale, d'autres moins chauds
    pour être dans des syndicats, d'autres qui ne font pas / savent pas la différence.

    La bonne idée serait de faire une association qui potentiellement peut
    accepter comme membre des syndicats.
    Si ça grossit, **transformer ça en fédération avec des antennes locales**.

    La première chose à faire pour s'organiser est de soumettre le choix de
    la structure sur le salon vote en mode asynchrone (voir Loomio), on
    pourra décider du mode de structure dans nos prochaines réunions.

:mgu: Personnellement, je suis syndiqué et si onestlatech devient
    un syndicat, je ne pourrais pas le rejoindre.

:Kev: Nous avons les choix suivants:

    - **Une association de faits** (nous en sommes déjà une, il n'y a rien
      besoin de faire pour changer)
    - **Une association de loi 1901** (pratique pour l'organisation, avoir
      un compte, et une structure)
    - Un syndicat (fonctionnement déjà détaillé, cela pose potentiellement
      problème pour les adhésions, et nous ne voulons pas être concurrents
      d'autres syndicats)


:Nic: Attention, l'organisation d'une association loi 1901 est une
    organisation très verticale (il y a un président, un bureau, un conseil
    d'administration, etc).

    Comment peut-on démocratiser la prise de décision dans ce genre d'orga ?

:pesar: C'est une bonne question à travailler sur le channel de
    discussions du :ref:`chantier 1 <chantier_1_latech>`

:maidu: On a parlé de sociocratie et d'holocratie (qui sont très
    complémentaires).

    Système avec role et plutôt qu'avec un vote majoritaire on fonctionne
    plutôt par objection : tant qu'on considère qu'une décision n'est pas
    nuisible pour l'organisation elle peut être prise (mais amendée etc).

    Plus d'infos : http://universite-du-nous.org/a-propos-udn/ses-outils/

    Je propose de faire une présentation du sujet, ça durerait environ 1h30.

    REX : dans ma boîte on utilise ça, les services qui le font sont très
    bons pour intégrer etc.

    Pour s'inscrire à cette présentation à propos de l'holocratie / sociocratie,
    elle aura lieu lundi entre midi et deux: https://pad.mehdiguiraud.info/p/OnEstLaTech_sociocratie

:Pevin: En ce qui concerne l'organisation du collectif, nous avions déjà
    préparé, il y a longtemps, une proposition pour ça. Je propose que nous
    votions déjà ce texte:

    https://github.com/onestlatech/onestlatech.github.io/pull/1110/files


Ca n'avance pas vite, alors avançons avec ce qu'on a déjà.


Infra et logistique interne
==================================

:Pevon: Deux personnes ont mis en place deux outils de vote:

    - Loomio: https://loomio.cafes-populaires.fr/onestla-tech/
    - Decidim: https://onestentest-decidim.herokuapp.com/users/sign_in


    Les discussions sur la gouvernance et l'organisation de notre collectif
    peuvent influer sur la configuration de ces outils.
    Alors testons les deux outils, et on pourra les adapter après.

    En attendant d'avoir un outil de vote asynchrone, on peut fonctionner
    de la manière suivante: on met tous les votes dans les chantiers en
    question.

    Règle : si tu ne participes au débat et les motions ne te plaisent pas,
    il fallait juste être la. Si tu ne participes au débat il ne faut
    pas te plaindre que les propositions mises dans la règle du jeu ne
    te plaisent pas.

:nuhdi:
    comment on décide en mode asynchrone et comment on décide quand on
    est tous ensembles. Il faut des règles de vote et qu'on s'y tienne
    pour que le vote fasse foi.

:rivon: Une association de faits, ça peut être trop arbitraire.
    On tourne en rond: on doit prendre des décisions, mais pour prendre
    des décisions, il faut qu'on soit d'accord sur lamanière de prendre
    des décisions, ce qui est une décision.
    Il faut déjà fixer qui peut voter, et comment.
    Je pense que tout le monde ne doit pas voter (ex. les gens pas assez
    investis), pour moi seuls ceux qui peuvent voter sont ceux qui
    participent et qui sont d'accord avec la vision du collectif.
    Par contre après le premier vote constituant, les inconnus et personnes
    peu investies risquent juste de perturber les votes.
    Il faut prendre une décision sur le droit de vote: membre d'une
    association, adhérent payée, présence minimum ? On veut éviter que
    les opportunistes nuisent au projet.

:kumian: comme on est à cheval entre les chantiers 1 et 2 et que ça va
    peut être être à cheval avec chantier 3. Je pars du principe que je
    ne suis pas payé, et que je suis volontaire: si ce n'est pas moi
    qui l'a fait, je considère que c'est bien fait.
    Au bout d'un moment si les gens ne sont pas la ou ne mettent pas du
    temps la dedans, ils n'ont pas  à venir emmerder ceux qui ont bossé.
    Pense que la majorité des gens resteront dans les clous du mouvement.

:xabeniin: Ok pour filtrer les vote uniquement pour les gens investis,
    mais tout le monde n'a pas le temps de venir à toutes les réunions.

:aline:
    Oui, laissons un peu de temps entre les votes pour arranger tout le monde.

:oekda:  Ok aussi pour laisser du temps pour le vote, par contre on va
    avoir besoin de décision rapide avec le temps.

:webin: Pour les sujets urgents, nous avons une Core Team qui, aujourd'hui,
    ont les accès sur plusieurs outils (site web, twitter, etc).
    Idéalement, ce serait bien de changer ce groupe de gens qui possède
    le pouvoir du collectif.
    Il faut laisser du temps pour les décisions de fonds, mais il faut
    aussi pouvoir réagir rapidement sur twitter, à l'actualité etc.
    Je propose de mandater des personnes qui puissent communiquer au nom
    du collectif, en accord avec des lignes directrices décidées collectivement.

    Il faut élire un groupe de personnes mandatées, avec certaines règles
    (ex: **50% de femmes minimum par mandat**)

    Pour les mandataires, il faut des règles: ligne claire décidée collectivement,
    toute action est publique avec un compte rendu ou au moins traçable,
    les mandataires doivent être révocables et doivent changer avec le temps.


Identité et valeurs du mouvement
==================================

.. seealso::

   - :ref:`chantier_3_latech`

Beaucoup de discussions, pour l’instant ça reste un peu brouillon.
Il y a pas mal de ressources à lire la sur le channel dédié sur Discord


:mavinluno: Écrivons un  draft de code de conduite interne avant de savoir
    comment s'exprimer en externe


Lobbying et positionnements publics
=========================================

:mecin: Affichons-nous, sur tous les canaux, partout.
    Je propose de nous afficher sur Twitch, et sur Twitter (les experts de
    la tech sont sur Twitter)

:palunti: Est-ce qu'on se positionne aussi sur les sujets non-tech ?

:pulime: Concentrons-nous plutôt sur la tech déjà ce sera un bel impact
    pour commencer (ref vers le chantier 3)

:pimual: Quels sont les sujets non-tech qu'on peut aborder ?
    Il y en a beaucoup.

:beto: Nous sommes une organisation militante, donc on peut se positionner
    sur tous les sujets importants de société, en particulier les sujets
    lié au militantisme, avec un prisme de la tech (sauf quand c'est pas
    possible). Gardons la liberté de réagir à chaud.

:mehin: La tech peut être un point d'entrée sur les sujets sociaux, nous
    sommes des pro de la tech, et la tech est partout aujourd'hui, donc on
    peut parler de tout.

    Si on ne veut pas passer juste des syndics déguisés, parlons comme comme
    des pro de la tech, mais avec une opinion sur les sujets de société.
    Parlons de la tech avec les sujets de sociétés que ça soulève.

:pifun: C'est important qu'on réagisse sur des gros sujets d'actualité,
    comme par exemple loi Avia (sujet abordé plus loin dans le CR)


Éducation populaire et vulgarisation
========================================

.. seealso::

   - :ref:`chantier_5_latech`

:wdis: J'ai participé avec paisyun. On a pas parlé des supports sur
    lequel faire de la vulgarisation, mais plus à qui nous pouvons nous
    adresser.

    On a fait une liste de personnes à contacter pour faire de la vulgarisation
    sur les fondamentaux de la tech, pour avoir ensuite une fondation solide,
    afin de pouvoir aborder d'autres sujets plus grave par la suite.

    Soyons grand public au début, même dans la tech tout le monde ne sait pas
    comment fonctionne un ordi, etc.
    Pas besoin de vote sur l'instant, on essaie déjà de décider ce sur quoi on va agir.
    Et tant qu'on a pas tout décidé collectivement de notre identité,
    fonctionnement, etc, communiquons sur la base, on avancera déjà sur ça.

    Il y a un listing de ressources d'info fiables dans le channel du
    :ref:`chantier 5 <chantier_5_latech>`, n'hésitez pas à participer
    et envoyer des liens.

:tvxon: Ne loupons pas l'éducation de la génération suivante.

    Tout support éducatif est forcement orienté, partons de la base pour
    pouvoir expliquer ces fondations selon notre vision (un ordi est fait
    de métaux rares par exemple, c'est pas souligné souvent).
    Ca peut permettre d'atteindre un public plus large.


Vision de la tech et éthique en entreprise
==================================================

.. seealso::

   - :ref:`chantier_6_latech`

:gxuè: Il y a une liste de textes fondateurs à lire sur le :ref:chantier 6 <chantier_6_latech>`
    On a pas mal parlé avec les designers éthiques, on a parlé des initiatives
    qui existent déjà, de numérique responsable, et de l'inclusion
    (**qui rejoint l'éthique**)

    Ce chantier rejoint un peu les autres: comment est-ce qu'on peut
    vulgariser l'impact du numérique sur la société, comment faire de
    la bonne pub, former les gens qui participent.

:dedimoine: J'ai eu un cas cette semaine, on devait valider une annexe à
    notre charte informatique et on en a parlé avec Richard Hanna, on a
    fait pas mal de propositions: l'idée c'est que ce ne soit pas juste
    une liste d'écogestes de la part des employés, mais aussi des actes
    concret de l'entreprise. On a un peu fait bouger ça.

    Par contre, je n'ai pas envie que onestla.tech devienne un trop
    orienté business, et j'étais gêné de faire de la promo, j'aimerai
    qu'il n'y ait pas de notion trop financière.

    Il faut faire attention à ne pas avoir une limite financière à accéder
    à la sobriété numérique.

    Moi je vais demander au CSE si on peut avoir une formation aux usages
    numériques, etc, et j'aimerai me tourner vers les gens de onestla.tech
    si possible mais que ce ne soit pas par intérêt financier.

    Par contre je veux bien partager ce qu'on a fait et en faire le suivi
    de cette action.


Création d'un label "onestlatech"
=====================================

.. seealso::

   - :ref:`chantier_7_latech`

:xulina: Ca n'a pas beaucoup avancé (les discussions sont restées vides
    sur Discord)

:mevon: Peu d'échange, fermons sujet: vous êtes d'accord, on veut pas
    faire de label ? (le chat a été d'accord)

   A la place, faisons plutôt une charte que les entreprises pourront signer

:aompld: Un label est difficile à administrer et coûteux en ressources

:Pano: Gardons un lien unique avec les entreprises: elles peuvent nous
    soutenir, mais qu'elles n'attendent rien en retour.


Actualité & Communication externe
===================================

:giwinli: Nous avons été contacté par l'ADN, il faudrait qu'un mandataire
    y réponde

    A propos de la notre position sur la loi Avia 2 (« contre les propos haineux »)
    et la loi **sécurité globale**  interdisant de publier des vidéos
    de violences policières sur les Réseaux sociaux ?

:pemoi: Donnons notre avis d'expert: ça sert à rien, et c'est liberticide.
    Soyons visible sur ces sujets là.

:Mercuze: Sur la PPL sécurité il y a plus que l'article 23 qui parle de
    l'interdiction de diffuser des visages de policier, mais aussi un
    passage sur les drones, plein de choses qui peuvent nous faire peur.

:Paula: La Quadrature du Net a déjà communiqué dessus, on peut réagir
    en disant qu'on est d'accord

:Anatole: Est-ce qu'il est pertinent de se positionner alors que nos chantiers
    d'identité, manifeste interne, etc, ne sont pas terminés ?
    Je pense qu'on peut attendre deux semaines le temps que nous soyons alignés

:Martin: Je suis pas du tout d'accord, notre groupe n'est pas figé, ce qui
    va nous construire et ce qui va forger nos valeurs, c'est justement
    notre réaction publique.
    Là il y a des sujets concrets qui permet de mobiliser les gens, ça doit
    être la priorité.

    Les sujets de fonds doivent avancer, mais l'actualité doit être la priorité.
    Je pense qu'on est déjà tous plus ou d'accord sur ces sujets.

:martha: Ouvrons le champ des possibles, et ne nous focalisons pas sur la
    réforme des retraites.

    Venez sur le :ref:`chantier 3 <chantier_3_latech>` pour bosser sur
    le sujet **Une autre tech est possible**.

    Ce sera la base de notre manifeste, du collectif, et de notre réaction
    à l'actualité prochaine [ndlr: des mandataires].


Organisation de la prochaine réunion
======================================

:sophie: Tout le monde ne peut pas venir si on ne se voit que tous les vendredi à 19h
    Si on change nos horaires, ça peut faire venir d'autres personnes, mais
    après on commence à s'habituer à ces horaires aussi

:bernard: Se réunir le vendredi soir c'est OK pendant le confinement, mais
    plus tard ce sera impossible, les gens rentrent chez eux le vendredi soir
    ou sortent en weekend.

    On peut peut-être fixer la date de réunion suivante à la fin de
    chaque réunion ?

:alex: Changeons de  jour une fois toutes les deux semaines pour
    que ça tourne un peu

:mathieu: Essayons les midi aussi ? Pas mal de boîtes font des BBL

:xavier: Faire une réunion le midi, ça avoir des réunions courtes.
    Pas d'AG le vendredi soir svp

:karla:  Ok, partons sur Mardi 17 novembre le 12h30 (1h~1h15 max)
    Pour les groupes, essayez d'avoir des sujets prêts pour pouvoir préparer
    l'ordre du jour le lundi soir pour mardi midi
