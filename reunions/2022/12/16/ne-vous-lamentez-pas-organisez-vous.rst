.. post:: 2022-12-16
   :tags: Organisation
   :category: Organisation
   :author: onestla.tech
   :location: France
   :language: fr


.. _organize_2022_12_16:

=========================================================================================
2022-12-16 **Ne vous lamentez pas, Organisez vous !** par Anne Lesouef et Kévin Douglas
=========================================================================================

- https://conf.onestla.tech/speakers/anne-lesouef/
- https://conf.onestla.tech/speakers/kevin-dunglas/
- https://dunglas.dev/2022/12/ne-vous-lamentez-pas-organisez-vous/

Intervention réalisée avec Anne Lesouef, coprésidente de l’Offensive et
présenté lors de la première conférence onestla.tech.

La tech a pris le pouvoir. Au cours des dernières décennies, le secteur
des nouvelles technologies est devenu la locomotive de l’économie mondiale.

La révolution de l’IT a permis à quelques ingénieurs devenus capitaines
d’industrie de s’enrichir comme jamais vu auparavant dans l’histoire de
l’humanité, et de le prendre le contrôle de l’ensemble des moyens de
communication de la planète, donc de sa vie politique.

Mais la tech a pris le pouvoir sur un monde mourant, sur un brasier qui
ne cesse de s’étendre sous nos yeux : les cataclysmes climatiques s’enchaînent,
les êtres vivants s’éteignent en masse, la pollution, la sécheresse,
la montée des océans rendent de plus en plus de régions inhabitables.

Les ingénieuses et les ingénieurs des années 80 avaient rêvé la technologie
comme libératrice. Ils ont inventé le logiciel libre, le web, les commons…

Mais les plus organisés d’entre-eux ont détruit cet espoir d’émancipation
dans l’unique but de faire le maximum de profits possible : surveillance
de masse, manipulation de l’opinion à l’échelle planétaire, algorithmes
et IA conçus pour perpétuer et accélérer l’exploitation et le racisme…

C’est pourtant cette course irraisonnée aux profits qui est en train
de détruire la planète. Notre seul espoir c’est de produire moins, de
produire mieux, de produire plus utile et de contrôler cette production
démocratiquement pour la mettre au service du bien commun.

En d’autres termes : sauver la planète c’est bâtir sans attendre le
communisme libertaire.

Aujourd’hui encore, les travailleuses et les travailleurs de la tech sont
en mesure de prendre le pouvoir.

Ils ont tous les atouts en main : ils disposent des méthodes les plus
efficaces, de l’ingéniosité nécessaire et de la capacité à automatiser
tout ce qui peut l’être.

Encore faudrait-il qu’ils comprennent que les milliardaires (qui les
licencient en masse comme de vulgaires ouvriers d’antan) ne sont pas
leurs alliés, et qu’ils s’organisent pour – cette fois-ci – vraiment
changer le monde.

« Don’t mourn, organize! »
