
.. _latech_actions_2022_12_27:

====================================================================
2022-12-27
====================================================================


Compte Rendu de la conférence onestla.tech
==============================================

Gros succès, à refaire dès que possible !
Beaucoup de contacts intéressants à péréniser

Structuration du collectif
=============================

Adhésions
-------------

- Ouvrir les adhésion sur Open Collective
- Demander à l’Offensive pour être note “hôte fiscal” (confédération militante
  à laquelle nous avons adhérer en septembre 2022)

Mandaté : Kévin

Moderniser le site
=====================

Refaire une beauté au site
Y mettre en avant le collectif plutôt que l’appel sur la page d’accueil

Mandaté : Hugo

Matériel militant
===================

Il faudrait du matériel militant (t-shirts, chasubles, stickers etc)
pour visibiliser le collectif au sein des évènements tech, des manifs…

Spreadshirt permet de le faire sans avancer d’argent (flux tendu).

Mandaté : Hugo

Présence dans les évènements tech
=====================================

Proposition d’organiser des RDV onestla.tech en marge des gros évènements tech de l’année.

Évènement clef avant (ou après) lesquels faire des pré-meetups :

- 16 au 18 février 2023 : Conférence Python Bordeaux (Mandaté : Arthru)
- 12 mai 2023: AFUP Day Lille et Lyon (Mandatés : Arthru pour Lyon, Kévin pour Lille)
- 31 mai 2023: Web2Day à Nantes (on cherche des volontaires pour organiser ça)
- 14 au 17 juin 2023: Vivatech à Paris (on cherche des volontaires pour organiser ça)

Réforme des retraites
===========================

- Mobiliser la tech
- Organiser des pré-RDV avant les manifestations qui auront lieu
- Écrire un tract ciblant spécifiquement les travailleuses et travailleurs de la tech (on cherche un·e volontaire)

Prochaine réunion : Mardi 31 janvier 2023
============================================

Mardi 31 janvier, en hybride présentiel + visio pour les gens ne pouvant se rendre sur place :

- à Nantes (chez la Scop Troopers) à Paris (chez la Scop Fairness)
- à Lille (au local de l’Offensive)
- on cherche des nouvelles villes ou donner d’autres RDV en présentiels

