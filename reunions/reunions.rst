
.. _onestlatech_actions:

====================================================================
Réus/actions
====================================================================


.. seealso::

   - https://www.arawa.fr/2020/05/12/bonnes-pratiques-de-la-visioconference/
   - https://ag.onestla.tech/compte-rendus/

.. figure:: prisedeparole.jpg
   :align: center

Interventions dans discord
=============================

Pour prendre la parole
------------------------

Tapez dans le chat:

- ! pour intervenir dans la discussion
- ? pour poser une question

Votre nom sera ajouté à la liste de Prise de Parole (PP) qui sera
publiée régulièrement dans le channel #discussions

Pour réagir sans parler
-------------------------

Tapez dans le chat:

- **++** pour dire qu'on est d'accord avec
- **--** pour dire qu'on est en désaccord avec
- **~~** pour dire qu'on est en plus ou moins d'accord avec
- **>>** pour dire que la prise de parole est trop longue

Réunions/Articles
=====================

.. toctree::
   :maxdepth: 3

   2022/2022
   2020/2020

