.. index::
   pair: blog; sphinx
   pair: blog; Chris Holdgraf
   ! meta-infos


.. _latech_meta_doc:

=====================
Documentation
=====================

.. seealso::

   - https://france1.frama.io/onestlatech/

.. contents::
   :depth: 3


Head project : tuto_devops
=============================

.. seealso::

   - https://gdevops.gitlab.io/tuto_devops/


Inspiration : sphinx-blogging de Chris Chris Holdgraf
========================================================

.. seealso::

   - https://predictablynoisy.com/posts/2020/sphinx-blogging/
   - https://gdevops.gitlab.io/tuto_ablog/posts/2020/10/10/sphinx_blogging.html
   - https://gdevops.gitlab.io/tuto_ablog/
   - https://gdevops.gitlab.io/tuto_documentation/

Gitlab project
================

.. seealso::

   - https://framagit.org/france1/onestlatech


Issues
--------

.. seealso::

   - https://framagit.org/france1/onestlatech/-/boards


pipelines
-------------

.. seealso::

   - https://framagit.org/france1/onestlatech/-/pipelines


Site
-----

.. seealso::

   - https://france1.frama.io/onestlatech/

root directory
===============

::

    $ ls -als

::

    total 176
    4 drwxr-xr-x 14   4096 nov.   7 14:54 .
    4 drwxr-xr-x  3   4096 oct.  31 07:30 ..
    4 drwxr-xr-x  4   4096 nov.   7 14:57 _build
    4 drwxr-xr-x  9   4096 nov.   7 14:56 chantiers
    4 -rwxrwxrwx  1   3900 nov.   7 14:54 conf.py
    4 drwxr-xr-x  2   4096 nov.   1 09:08 ethique
    4 -rw-r--r--  1     98 oct.  31 07:55 feed.xml
    4 drwxr-xr-x  8   4096 nov.   7 15:03 .git
    4 -rwxrwxrwx  1     49 mai   31 10:41 .gitignore
    4 -rw-rw-rw-  1    214 août  19 17:43 .gitlab-ci.yml
    4 drwxrwxrwx  2   4096 nov.   7 10:28 glossaire
    4 drwxr-xr-x  2   4096 oct.  19 18:09 index
    4 -rwxrwxrwx  1    800 nov.   7 14:51 index.rst
    4 -rw-r--r--  1   1177 nov.   6 08:46 Makefile
    4 drwxr-xr-x  2   4096 oct.  31 12:17 manifeste
    4 drwxr-xr-x  4   4096 nov.   7 07:33 meta
    84 -rw-r--r--  1  85325 nov.   6 19:59 poetry.lock
    4 -rw-rw-rw-  1   1141 nov.   6 08:47 .pre-commit-config.yaml
    4 -rw-rw-rw-  1    338 oct.  26 17:44 pyproject.toml
    4 -rw-rw-rw-  1   2019 nov.   7 07:18 requirements.txt
    4 drwxr-xr-x  8   4096 nov.   2 08:58 ressources
    4 drwxr-xr-x  3   4096 nov.   7 15:02 reunions
    4 drwxr-xr-x  2   4096 nov.   4 19:07 _static
    4 drwxr-xr-x  3   4096 oct.  31 16:41 syndicats


pyproject.toml
=================

.. literalinclude:: ../../pyproject.toml
   :linenos:

conf.py
========

.. literalinclude:: ../../conf.py
   :linenos:


gitlab-ci.yaml
===============

.. literalinclude:: ../../.gitlab-ci.yml
   :linenos:


.pre-commit-config.yaml
========================

.. literalinclude:: ../../.pre-commit-config.yaml
   :linenos:


Makefile
==========

.. literalinclude:: ../../Makefile
   :linenos:



Arborescence de la documentation
===================================

::

    make tree

.. literalinclude:: ../../tree.txt
   :linenos:
