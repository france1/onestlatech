.. index::
   ! Chantier 5
   pair: Framapd ; Gouvernance et structure interne

.. _framapad_chantier_5_latech:

======================================================
Framapad
======================================================

.. contents::
   :depth: 3


Framapad
=========

.. seealso::

   - https://hebdo.framapad.org/p/ch5-sitemalveillant-9jza?lang=fr


Les sites malveillants
=============================

Nos sources documentaires

- https://support.mozilla.org/fr/kb/comment-fonctionne-protection-contre-hame%C3%A7onnage-et-logiciels-malveillants
- https://support.google.com/chrome/answer/99020?co=GENIE.Platform%3DDesktop&hl=fr
- https://www.cybermalveillance.gouv.fr/

Fond du sujet
================

Définition(s)

Problématique(s) autour du sujet

"Quels sont les objectifs d'un site malveillant ?"

- un site qui cherche qui sert à transmettre un moyen de ou à prendre le contrôle d'un ordinateur
- un site qui a pour but de voler/engranger des données personnelles d'un utilisateur
- un site qui a pour but de véhiculer des informations fausses, erronées (bon ce point est plus discutable ...)
  "Comment reconnaître un site malveillant ?"
- url et/ou design proche d'un site légitime (parfois au caractère près dans l'url)

"Comment se protéger d'un site malveillant ?"
-------------------------------------------------

Supports de diffusion
BD Courte -  Y a quoi sur mon écran ?

Reprise de l'idée de @Volubyl d'un short comic strip où l'on aurait
entre 3 et 6 cases de vulgarisation dur les problématiques au dessus.
Elles pourraient être traduites pour ce concept par :

- Maman, pourquoi l'ordinateur me dit que ce site est malveillant ?
- Fiston, j'ai ce message d'erreur "Site Malveillant", je fais quoi ?
- Mamie, si je vais sur ce site j'aurais un virus ?

Infographie animée - La minute technique
=============================================

Vidéo d'une minute dont le but est de répondre à l'une des problématique
en se concentrant sur l'essentiel. Cette vidéo doit être partageable
assez facilement pour qu'elle soit vue pendant la consultation de nos
réseaux sociaux préférés ;)

Ne doit pas être la plus exacte mais (économie de l'attention oblige)
doit être suffisamment intrigante pour inciter le spectateur à se
rediriger vers un support un peu plus complet.

Guide illustré - "Une feuille blanche pour vulgariser"
==========================================================

Un peu plus complet que l'infographie animée, le but est de vulgariser
un concept dans une feuille A4 afin que sur écran la lecture ne soit
pas trop longue, et que sur papier l'information puisse être imprimée
par des associations par exemple.


