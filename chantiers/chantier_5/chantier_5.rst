.. index::
   ! Chantier 5
   pair: Chantier; Education populaire et vulgarisation

.. _chantier_5_latech:

======================================================
Chantier 5 **Education populaire et vulgarisation**
======================================================

.. contents::
   :depth: 3


Origine
=========

.. seealso::

   - :ref:`chantier5_2020_10_30`
   - https://mensuel.framapad.org/p/onestlatech-9jt6?lang=fr

Description
=============

- Divers publics, personnes jeunes, personnes âgées, personne peu à
  l'aise avec le numérique...
- Fracture numérique
- Vulgarisation grand public (exemple des RS et des données collectées
  à expliquer à nos proches, ce genre de choses)


Ressources
===========

.. toctree::
   :maxdepth: 3

   ressources/ressources

TODO
=====

- https://www.bortzmeyer.org/ (pas grand public)
- https://choosealicense.com/
- https://nomoregoogle.com/
- https://guide.boum.org/
- https://www.helloruby.com/
- https://speakerdeck.com/dunglas/petite-introduction-a-lauto-defense-numerique
- https://parisabc.noblogs.org/atelier-dautodefense-numerique/
- https://zestedesavoir.com/
