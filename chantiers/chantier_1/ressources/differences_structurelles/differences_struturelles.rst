.. index::
   pair: Chantier 1 ; différentes structures d'organisation

.. _differentes_structures:

================================================
**Les différentes structures d'organisation**
================================================

.. contents::
   :depth: 3

Page framapad
=====================

.. seealso::

   - https://hebdo.framapad.org/p/R%C3%A9capitulatif_des_diff%C3%A9rences_structurelles-9jv3?lang=fr


Le syndicat
============

Définition
------------

Un syndicat est un groupement de personnes physique et/ou morale pour la
défense d'intérêts communs.

Ils peuvent porter un projet politique de société et nulle personne ne
peut être inquiétée pour son affiliation à tel ou tel syndicat.

Créer un syndicat
-------------------

Aujourd'hui la création d'un syndicat national ne semble plus possible
et la création d'une section syndicale d'entreprise est assujettie au
rattachement à une centrale d'un syndicat national préexistant depuis
au moins deux ans et/ou à une organisation syndicale représentative.

Source ci dessous: http://www.cnt-f.org/ul33/creation-dune-section-syndicale-prive/

La constitution d’une section syndicale est depuis la nouvelle loi
ouverte à tous les syndicats, y-compris les syndicats non représentatifs
nationalement ou dans la branche d’industrie concernée, avec trois
exigences (Code du travail, art. L. 2142-1) :

- l’indépendance ;
- le respect des valeurs républicaines
- être affilié à un syndicat légalement constitué depuis au moins deux
  ans et comprenant l’entreprise dans son champ professionnel et géographique

Les moyens d'actions d'un syndicat
-----------------------------------

.. seealso:: https://www.vie-publique.fr/fiches/24063-quel-est-le-role-dun-syndicat

Les syndicats sont un élément essentiel de la défense efficace d'un
salarié dans le secteur privé. Ils offrent assistance, conseil et
protection à leurs adhérents si un conflit entre les salariés en cas
de conflit avec l'employeur, il peuvent lancer des préavis de grève
et élire des représentants afin de participer aux intersyndicales.

Financement
------------------

Le financement des syndicats est assuré de plusieurs façons :

- **par les cotisations que les adhérents versent à leur syndicat**, selon
  un barème établi proportionnellement à leur salaire ;
- par les entreprises qui peuvent également contribuer financièrement
  à l’activité syndicale ; ces subventions au titre de l’exercice du droit
  syndical sont distribuées de façon égalitaire entre syndicats
  représentatifs ou sont proportionnelles aux résultats électoraux ;
- par les collectivités locales qui peuvent allouer des subventions
  aux unions locales de syndicats ;
- par des subventions publiques destinées à financer certaines activités
  syndicales (formation des conseillers prud'hommes du collège des salariés,
  formation syndicale…).

Par ailleurs, certaines des activités ou frais de fonctionnement des
syndicats sont pris en charge de diverses façons, par exemple par la
mise à disposition gratuite de locaux par les collectivités (notamment
les bourses du travail).

:Source: https://www.vie-publique.fr/fiches/24064-comment-est-finance-un-syndicat

Statut et responsabilité juridique
----------------------------------------

.. seealso::

   - https://www.legifrance.gouv.fr/codes/id/LEGIARTI000006649567/2001-02-20/
   - https://www.legifrance.gouv.fr/codes/section_lc/LEGITEXT000006072050/LEGISCTA000006170315/2001-02-20/#LEGISCTA000006170315
   - https://www.legifrance.gouv.fr/codes/section_lc/LEGITEXT000006072050/LEGISCTA000006170316/2001-02-20/#LEGISCTA000006170316

Un syndicat a donc une **personnalité civile** ainsi qu'une responsabilité
civile.

**La personnalité civile** appartient à tout groupement pourvu d'une
possibilité d'expression collective pour la défense d'intérêts licites,
dignes par suite d'être juridiquement reconnus et protégés.

Ils peuvent donc saisir la justice dans le cadre du respect de leur
statuts et de leur fonction.
Ils peuvent posséder des biens fonciers et matériels qui sont
insaisissables si nécessaires à leurs réunions, à leurs bibliothèques
et à leurs cours d'instruction professionnelle.

En complément le document de Nas:  https://annuel2.framapad.org/p/onestlatech-syndicat-9jt7?lang=fr


L'association de fait
=========================

Définition
-------------

L’association "de fait" ou "non déclarée" est un groupement de personnes
(physiques ou morales) qui n’a pas souhaité accomplir les formalités
de déclaration.

Créer une association de fait
------------------------------

Il n'y a aucune formalité à remplir, un groupement de personnes ayant
le même but et s'entendant sur le mode de fonctionnement de celle ci
suffit à créer une association de fait.

Les moyens d'actions d'une association de fait
------------------------------------------------

Ils sont la somme des moyens d'action individuels de ses membres.

Financement
----------------

Une association de fait ne peut recevoir ni subvention, ni legs, ni don.

Elle ne peut ouvrir de compte à son nom ni signer de bail de location,
ni obtenir de patrimoine.

Statut et responsabilité juridique
--------------------------------------

L’association non déclarée ne bénéficie pas de la capacité juridique de
la personne morale.

L’association non déclarée ne peut être assignée en justice
(Cour de cassation ; Soc. 12 juillet 2010, n° 09-41.402)

Son nom ou sa dénomination ne peuvent être protégés.


L'association déposée
========================

Définition
-----------

L'association déposée régie par la loi du premier juillet 1901, est un
groupement de personnes (physiques ou morales) s'étant par des formalités
administratives

Créer une association déposée
-------------------------------

L’article 5 de la loi décrit la procédure de déclaration "...toute
association qui voudra obtenir la capacité juridique prévue par
l’article 6 devra être rendue publique par les soins de ses fondateurs..."

Et l’association n’est rendue publique que par une insertion au Journal
officiel, sur production du récépissé de déclaration (L. 1er juill. 1901,
art. 5 al. 4).

:source: https://www.associations.gouv.fr/l-association-regie-par-la-loi-du-1er-juillet-1901.html

La personne qui procède à cette déclaration demande la publication de
cet extrait au JOAFE et s’engage à régler le montant des frais d’insertion.

Ces frais sont fixés par un arrêté du 19 novembre 2009 modifié :

- déclaration de création d’association, forfait : 44 euros ;
- déclaration de modification d’association, forfait : 31 euros ;
- pour les déclarations d’associations dont l’objet ou le nouvel objet
  publié dépasse 1 000 caractères, forfait : 90 euros.

A noter que si l'association est domiciliée en Alsace-Moselle des
dispositions particulières s'appliquent (essentiellement le fait que
le nombre de personnes nécessaires pour constituer l'association passe
de 2 à 7)

Les moyens d'actions d'une association déposée
---------------------------------------------------

Les moyens d'une association sont définis par ses statuts et ne sont
limités qu'au cadre strict de la loi en vigueur.

Financement
-------------

Les associations peuvent recevoir des legs, des dons et des subventions.
Ces acquis peuvent être sous forme d'actifs ou en nature.

Statut et responsabilité juridique
------------------------------------


La fédération associative
==============================

Définition
-------------

Une fédération associative est un groupement de personnes morales exclusivement.

Il ne semble pas possible de créer une fédération "de fait", toutes les
sources consultées appuient la nécessité d'une déclaration en préfecture.

Les syndicats peuvent adhérer à une fédération en qualité de personnes
morales si les buts des deux groupements concordent.

Créer une fédération associative
-----------------------------------

Au même dispositif qu'une association déclarée

Les moyens d'actions d'une fédération
--------------------------------------

Ils sont définis par ses status et ne sont limités qu'au cadre stricte
de la loi en vigueur.

Financement
------------

Les fédérations peuvent recevoir des legs, des dons et des subventions.

Ces acquis peuvent être sous forme d'actifs ou en nature.

Statut et responsabilité juridique
------------------------------------


L'association internationale ou ONGI
==========================================

Définition
-------------

Créer une ONGI
--------------

Les moyens d'actions d'une ONGI
----------------------------------

Financement
-------------

Statut et responsabilité juridique
-------------------------------------
