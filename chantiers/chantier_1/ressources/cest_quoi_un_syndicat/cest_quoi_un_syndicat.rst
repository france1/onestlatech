.. index::
   pair: Chantier 1 ; C’est quoi un Syndicat

.. _latech_syndicat:

================================================
**C’est quoi un Syndicat**
================================================

.. contents::
   :depth: 3

Page framapad
=====================

.. seealso::

   - https://annuel2.framapad.org/p/onestlatech-syndicat-9jt7?lang=fr

C’est quoi un Syndicat
============================

Vis à vis d’une association ou d’une coopérative
----------------------------------------------------

En Allemagne, un syndicat est une association ayant pour objet la défense
des travailleuses et des travailleurs. Il n’y a donc, à priori, pas de
différences avec une association.

Juridiquement, en France un syndicat dispose de plusieurs capacités
particulières : celles de représenter juridiquement les salariés, est
proche aux associations de défenses d’intérêts.

Cependant, ce statut garantit aussi un **certaine anonymat**, y compris
vis-à-vis de la police, et de liberté d’expression.

:XXX: Aurais-tu des sources à ce sujet ? On peut effectivement
    adhérer anonymement à un syndicat (c'est à dire que l'employeur ne
    peut exiger du syndicat la liste des membres, mais l'adhésion à une
    asso peut être anonyme également) ;
    les locaux syndicaux bénéficient d'une certaine protection juridique,
    mais concernant l'anonymat vis à vis de la police ça m'intéresse
    je n'en avais jamais entendu parler avant.

C’est lié a son rôle historique de lanceur d’alertes.

Si l’adhésion à un syndicat peut-être pseudonyme, la reconnaissance par un
syndicat d’un action ou d’une parole permet de protéger un·e salarié·e·s
dans le cadre de ses revendications ou de son expression.

La différence avec une association, en plus des protections des données,
est simplement la plus grande facilité à se faire reconnaître comme
compétent sur le lieu de travail.
À l’inverse, cela ne discrédite pas pour autant un syndicat en dehors
de celui-ci.

Pour les syndicats qui sont d’accord pour participer, il est possible
d’être élus en tant que salariés, et d’avoir accès à des informations
protégées, comme les dépenses de l’entreprise.

Ces prérogatives changent en fonction du type de structure.

Les universités par exemple doivent soumettre aux votes des représentants
syndicaux certaines décisions.

Un syndicat permet aussi de gérer des formations syndicales.

Les salarié·e·s —syndiqué·e·s ou non— peuvent suivre sur leur temps de
travail des formations proposées par un syndicat.

Dans le privé, il y 12 jours de formations par an, 14 pour les elu·e·s.

L’employeur n’a pas le droit de refuser, ni à connaitre le contenu de
la formation, et læ salarié·e est payé normalement pendant cette formation.

Dans la définition, un syndicat reste une association de défense des
intérêt de ses membres (copro, patronat, etc…).

**Beaucoup d’associations sont en fait des syndicats**, mais n’en ont
simplement pas le statut, où ne s’en réclament pas.

**Du coup un syndicat n’a pas d’autres champs d’actions propres que ses
statuts**.

D’ailleurs le premier syndicat national (la fédération des bourses du travail)
avait autant le rôle de mettre en relation travailleu·r·ses et patrons,
de former le prolétariat, que d’aider à la constitution de coopératives.

Ces différents rôdes avaient plusieurs avantages : fixer les salaires et
les conditions de travail collectivement, permettre un transmission des
savoir-faires parmi celles et ceux qui défendent les mêmes intérêts,
et faire en sorte que les coopératives soient une alternative au salariat,
et non une concurrence.


Des définitions différentes
============================

Historiquement
----------------

Si historiquement les syndicats —très proches des anarchistes— sont créés
dans un objectif de changer le système productif dans l’intérêt de toutes
et tous, en évitant de se faire récupérer par les partis politiques.

Ce que explique le fonctionnement dont ils héritent :  des syndicats par
la base, *fédérés par savoir-faire**, et **confédérés sur un territoire**.

C’est évidemment ironique vu leur développement futur : le parti communiste,
puis socialiste, qui à force de noyautage ont détourné ces organisations.

Aujourd’hui, les centrales syndicales reflètent ces visions
particulières: un centralisme des négociations, voire de la cogestion.

De transformation sociale
-------------------------------

Les premiers syndicats ont pour objectif de **changer radicalement la
société depuis son système productif**.

Il s’agit de faire la révolution via la grève et l’autogestion des
entreprises, à la place de l’insurrection, suite à l’expérience
de la commune de 1871.
Elle sera illustrée en Espagne avec l’expérience libertaire de 1936 en
Catalogne et en Aragon.

Dans la seconde moitié du XXe siècle la CGT impose une vision plus
autoritaire de cette transformation sociale.
Si elle fait toujours partie des syndicats de luttes, ça la différencie
des syndicats par démocratie direct (SUD SOLIDAIRE) ou autogéré (CNT).

Ce positionnement explique pourquoi les syndicats s’expriment sur le
fascisme, l’écologie, l’exil, et d’autres sujets de sociétés.

Si à priori la charte d’Amiens interdit aux syndicats de se mêler de
politique, il s’agit en fait d’une volonté de se distancer des partis
politiques.

De cogestion
--------------

D’autres syndicats veulent réformer le capitalisme.

Il ne s’agit alors que de mutualiser les services (juridique, de formation)
aux salarié·e·s.
Généralement, ils abandonnent leur rôle de lanceurs d’alertes, puisqu’ils
cherchent à travailler avec les dirigeants, et non avec le reste de la
société. C’est le cas de la CFDT.

:XXX: Malheureusement oui. On peut mentionner que ça n'a pas toujours
    été le cas et que la CFDT a mené des expériences extrêmement
    intéressantes en matière d'autogestion jusqu'à il n'y a pas si longtemps,
    mais que désormais la plupart des militants "sincères" sont partis
    dans d'autres structures (et en particulier SUD).
    Exemple : https://autogestion.asso.fr/les-lip-quarante-ans-apres/

Même dans ces structures, les personnes à la base qui sont souvent plus
revendicatives que leur centrale, au point d’être trahit par elle.

On peut citer la CFDT qui a dissout et volé l’argent d’un de ses syndicat
qui ne voulait pas signer un accord avec son employeur.

Est ce qu’au final on s'en balance non ?
----------------------------------------------

La forme juridique du syndicat est un outil qui est objectivement
meilleur que les association sur ce plan, puisqu’elle permettent plus.

Par contre les centrales syndicales ont du mal avec la diversité.

Il faut donc imposer un rapport de force avec elles. Elles ont déjà
sabotées des initiatives similaires.
On peut citer des associations de luttes qui gagnent des luttes
(Droit au Logement, Dissenssus, La Quadra).

En général ce qui est déterminant c’est le rapport de force.

À savoir sur les syndicats
-----------------------------

Bien que peu de confédérations soient encore dirigées par la base, elles
en héritent la **forme confédérale** : des syndicats à la base sont
autonomes sur une grande partie de leurs activités.

Ils se **fédèrent** avec les syndicats partageant le même **savoir-faire**
(généralement des industries ou des métiers), et s’unissent sur leur
zone géographique.

On a donc :

- Syndicat
- Union Local (généralement les syndicats d’une ville)
- Union Départementale
- Union Régionale
- Fédération de l’industrie ou du métier du syndicat

Il y a des syndicats de métallurgie, d’éducation, mais à défaut il peut
exister des syndicats interprofessionnels (appelés aussi intercos), qui
permettent une activité pour les personnes n’étant pas assez nombreuses
pour former un syndicat.

Car s’il faut être 2 pour juridiquement former un syndicat, ça peut-être
insuffisant pour être visible (rédiger des tracts, former des cortèges,
tenir des permances syndicales, …).

Les syndicats tiennent souvent des permanences, afin de conseiller et
aider les salarié·e·s et de permettre leur adhésion.

Il n’échappera à personne la culture Marxienne, voir marxiste des
syndicats de luttes.
Elles impliquent une solidarité entre les travailleuses et travailleurs.

Ça implique qu’à la base, les syndicats de lutte peuvent vous aider
si vous êtes manifestement dans une démarche de lutte des classes.

Des Confédérations Syndicales travaillent régulièrement ensemble,
comme pour organiser le 1er Mai, tout comme leur fédérations.
Par exemple lorsque les personnels soignants appellent à la grève,
ielles le font au travers d’une réunion des représentant·e·s locaux
des fédérations des personnels soignants.

Dans le premier cas, comme le second, on appelle ça l’intersyndicale.

Critique de la forme syndicale
----------------------------------

Un syndicat est une organisation. Il y a donc des luttes et des abus de
pouvoirs.

Beaucoup de critiques des syndicats pointent les trahisons de leurs
bureaux parisiens, et pourraient être étendues à des associations de
luttes ou des partis.

On reproche aussi le productivisme aux syndicats, si c’était vrai des
tendances majoritaires, ça l’est de moins en moins avec la montée du
syndicalisme vert dans les confédérations. S’organiser sur la question
du travail n’empêche pas sa critique, au contraire.

Critique et limites du syndicalisme (et en particulier des centrales)
dans le cadre du mouvement de lutte contre la réforme des retraites
et à la suite du mouvement des Gilets Jaunes : https://acta.zone/pour-une-gilet-jaunisation-du-mouvement-social/

Analyse sur (entre autre) des syndicats ouvriers et de la gauche
révolutionnaire par Bookchin (un point de vue écologiste / municipaliste libertaire) : https://blog.agone.org/post/2019/10/07/Sortir-de-l-impasse-%28II%29

Critique des syndicats du point de vue de l'autonomie ouvrière: https://www.infolibertaire.net/autonomie-ouvriere-syndicat/


---
1. https://fr.wikipedia.org/wiki/Charte_d%27Amiens
2. https://fr.wikipedia.org/wiki/Conf%C3%A9d%C3%A9ration_fran%C3%A7aise_d%C3%A9mocratique_du_travail#Condamnations
