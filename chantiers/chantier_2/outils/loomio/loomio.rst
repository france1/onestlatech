.. index::
   pair: Chantier 2; loomio
   pair: Outil; loomio
   ! loomio

.. _loomio:

============================================================
**loomio** Loomio helps people make decisions together
============================================================

.. seealso::

   - https://github.com/loomio
   - https://github.com/loomio/loomio
   - https://github.com/loomio/loomio/graphs/contributors
   - https://twitter.com/Loomio
   - https://ag.onestla.tech/explore?order=memberships_count


.. contents::
   :depth: 3

Loomio a été adopté lors de la réunion du vendredi 6 novembre 2020
===================================================================

.. seealso::

   - :ref:`decisions_2020_11_06`

Loomio a été :ref:`adopté lors de la réunion du vendredi 6 novembre 2020 <decisions_2020_11_06>`.

loomio.coop
============

.. seealso::

   - https://loomio.coop/


The software we build is open source, and our organisation is open source
too.

This handbook is where we document how we run an efficient organisation
without a hierarchy.

We build software for collaborative decision making, used by thousands
of organisations and communities around the world. If you’re looking
for a way to make group decisions without meeting, try it out at
loomio.org.

Versions
==========

.. toctree::
   :maxdepth: 3

   versions/versions
