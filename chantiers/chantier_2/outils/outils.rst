.. index::
   ! Chantier 2
   pair: Outils; Infra et logistique interne
   ! Outil

.. _outils_latech:

============================================================
Outils
============================================================

.. toctree::
   :maxdepth: 3


   decidim/decidim
   discord/discord
   matrix/matrix
   loomio/loomio
   padlet/padlet
