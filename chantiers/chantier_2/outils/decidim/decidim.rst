.. index::
   pair: Chantier 2; decidim
   pair: Outil; decidim
   ! decidim

.. _decidim:

============================================================
**decidim** (The participatory democracy framework)
============================================================

.. seealso::

   - https://github.com/decidim
   - https://github.com/decidim/documentation
   - https://github.com/decidim/decidim/graphs/contributors
   - https://decidim.org/
   - https://twitter.com/decidim_org

.. contents::
   :depth: 3

meta decidim
============

.. seealso::

   - https://meta.decidim.org
