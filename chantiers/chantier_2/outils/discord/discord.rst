.. index::
   pair: Chantier 2; discord
   pair: Outil; discord
   ! discord

.. _discord:

============================================================
**discord** pas libre mais utilisé
============================================================

.. seealso::

   - https://fr.wikipedia.org/wiki/Discord_%28logiciel%29
   - https://fr.wikipedia.org/wiki/Discord_%28logiciel%29#Controverse
   - https://onestla.tech/page/a-propos/#nos-incohrences


.. contents::
   :depth: 3

Historique
============

:x: Vous trouvez pas ça chelou d'être sur Discord pour organiser "Une autre tech"?

:y: on parle depuis longtemps de passer à Matrix, sauf que personne n'a
    pris le temps d'installer un serveur et de configurer un bridge
    avec Discord jusqu'ici, et qu'on ne souhaite pas perdre le monde
    qui est déjà sur ce serveur

:z: L'avantage de Discord étant quand même que ce serveur est à 1 clique
    de portée pour beaucoup de gens, notamment pour ceux pas encore convaincus

Caractéristiques
===================

Discord est un logiciel propriétaire gratuit de VoIP conçu initialement
pour les communautés de joueurs.

Il fonctionne sur les systèmes d’exploitations Windows, macOS, Linux,
Android, iOS ainsi que sur les navigateurs web.

La plateforme comptabilise le 21 juillet 2019 plus de 250 millions
d'utilisateurs.

En 2019, l’entreprise emploie 165 salariés à San Francisco et est
valorisée à 3,5 milliards de dollars.

Controverse
============

.. seealso::

   - https://fr.wikipedia.org/wiki/Discord_%28logiciel%29#Controverse

Malgré la mise en place de protections (notamment une analyse des fichiers
téléversés par Virustotal), Discord est manifestement utilisé pour
propager et administrer des logiciels malveillants.

Il est en effet possible de se servir des serveurs Discord comme moyen
d'hébergement et de distribution de fichiers malveillants.

De plus, le logiciel peut être modifié de manière à envoyer les
informations confidentielles (numéro de téléphone, moyens de paiement,
contenu du presse-papier…) des utilisateurs à un serveur distant28.

Le 29 octobre 2019, la cellule d'intégration de la cybersécurité et des
communications (en) du New Jersey produit une fiche événementielle liée
à la découverte, par MalwareHunterTeam, d'un malware sur Discord
utilisable pour le vol d'adresse e-mail, numéro de téléphone,
nom d'utilisateur, adresse IP et paramètres utilisateur Discord.

Ce malware, nommé BlueFace, copie également les 50 premiers caractères
du presse papier et est ainsi susceptible de voler les mots de passe
utilisateurs.

Enfin, ce malware crée une porte dérobée pour une infection ultérieure


