.. index::
   ! Chantier 4
   pair: Chantier; Lobbying et positionnements publics
   pair: Chantier 4; Logiciel libre
   pair: Chantier 4; Communs

.. _chantier_4_latech:

============================================================
Chantier 4 **Lobbying et positionnements publics**
============================================================

.. contents::
   :depth: 3


Origine
=========

.. seealso::

   - :ref:`chantier4_2020_10_30`
   - https://mensuel.framapad.org/p/onestlatech-9jt6?lang=fr


Description
=============

- Se positionner sur de grands enjeux de société (5G etc)
- S'incruster dans les conférences pour faire entendre nos voix,
  s'exprimer sur ces sujets
- **Militer pour que l'Etat utilise forcément et donc investisse dans le
  libre** = **pour un service public du  logiciel libre et des communs**
- Militer pour un droit de retrait tech lié à notre éthique (objection
  de conscience)
- **Shaming** : noter les entreprises en fonction des valeurs du collectif
  (logiciels libres, tracking, salaires, diversité...))


TODO
======


open-data
-----------

- https://www.mission-open-data.fr/processes/politique-publique-donnee/f/2/proposals/91


Sur le nucléaire
---------------------

:ricci: Sur le nucléaire, je pense pas qu’on puisse trouver de consensus pratique,
    mais on devrait peut-être trouver chercher à s’accorder sure une positions
    plus générale, quitte à affiner plus tard :
    « une consommation énergétique qui ne soit pas oppressive sur les individus,
    quelque soit leur position dans  l’espace (partout dans le monde) ou le
    temps (aujourd’hui et dans le futur) »

    Est-ce que vous seriez d’accord avec une proposition comme celle-ci ?
    Et est-ce que vous êtes d’accord avec la méthode : en cas de désaccord,
    chercher le principe plus général qui fait consensus ?




:paul: Pas oppressive dans l'espace et le temps ? Ça ne veux pas dire grand chose
    si ? Que l'on soit proENRi, proNuc, ou proFossiles (j'espère qu'il n'y en a pas )
    chaque technologie oppresse plus ou moins l'humanité. Le fossiles avec...

    Le fossile et les émission directes et indirectes, les ENRi avec les
    ressources utilisées qui ne sont ni renouvelables, ni (encore) recyclable,
    le nuc avec les déchets qui ne sont que trop peu réutilisables .

    Sachant que toutes détruisent plus ou moins la biodiversité de manière
    directe et indirecte.
    Pareil pour parler du futur, c'est très délicat de trouver une
    technologie qui n'opresse pas l'humanité dans le temps, les ENRi pompent
    de plus en plus de ressources rares, le nuc d'uranium, et les deux créent
    indirectement des macro-économies miliciennes qui influent sur les
    personnes les extractants. (du coup niveau oppression dans le temps on
    est pas bon) Et le fossile, bah heu...
    Ce sont des émissions directes et indirectes de C02 dans l'atmosphère
    pour 60 à 90 ans...

    Je suis pour ton idée d'adopter une position globale pour laisser le
    débat ouvert : on peut peut-être parler de 'moins oppressive' au lieu
    du 'pas oppressive' que je trouve utopiste et un poil mensonger. (#noGreenwashing)
