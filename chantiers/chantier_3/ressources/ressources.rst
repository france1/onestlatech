.. index::
   pair: Ressources; Identité et valeurs du mouvement

.. _ressources_chantier_3:

============================================================
Ressources
============================================================

.. toctree::
   :maxdepth: 3

   framapad/framapad
   rfcs/rfcs
