.. index::
   pair: Framapad; Vision de la tech et éthique en entreprise

.. _chantier_6_framapad:

========================================================================
Framapad
========================================================================


.. seealso::

   - https://annuel2.framapad.org/p/ew4lb5grz7-9jzm?lang=en


- Bullshit jobs de David Graeber: https://fr.wikipedia.org/wiki/Bullshit_jobs http://www.editionslesliensquiliberent.fr/livre-Bullshit_Jobs-9791020906335-1-1-0-1.html
- Robotisés, rebelles, rejetés ? Maîtriser les nouvelles technologies, par Yves Lafargues
  Editions Ouvrières, Collection "Portes ouvertes", 1993: https://sisr.swissinformatics.org/robotises-rebelles-rejetes-yves-lasfargues-1993/


- Le Serment Holberton-Turing a ete initié dans l'objectif de fédérer
  et mettre en conscience tout professionnel du domaine de l'Intelligence
  Artificielle, au niveau mondial, autour de valeurs morales et éthiques
  communes afin de les inviter à utiliser leurs compétences dans le
  respect de l'humain en évitant toute menace à la vie
  https://www.holbertonturingoath.org/accueil

- Guerilla Open Access Manifesto  par Aaron Swartz July 2008, Eremo, Italy
  https://framablog.org/2013/01/14/manifeste-guerilla-libre-acces-aaron-swartz/

- Serment d'hippocrate pour les data scientists https://hippocrate.tech/
- https://2i2c.org/right-to-replicate/

Textes et livres fondateurs onestla.tech
========================================

Manifestes
============

Écrits par le collectif
--------------------------

- L'appel fondateur : https://onestla.tech/
- L'appel "Une autre tech est possible" : https://onestla.tech/publications/une-autre-tech-est-possible/


Manifestes importants
-----------------------

- Manifeste écologique des professionnel·le·s de l’informatique refusant
  de travailler pour les grandes entreprises polluantes type Total, SG...
  (lancé entre autre par des membres fondateurs du collectif) : https://www.climanifeste.net/
- Déclaration d’indépendance du Cyberespace : http://morne.free.fr/celluledessites/OeilZinE/declarationdindependanceducyberespace.htm / https://www.eff.org/fr/cyberspace-independence
- Code Is Law : https://framablog.org/2010/05/22/code-is-law-lessig/
- La Netiquette : https://tools.ietf.org/html/rfc1855
- The Internet is for End Users : https://www.rfc-editor.org/rfc/rfc8890.html
- Hacker Manifesto : https://fr.wikipedia.org/wiki/Un_manifeste_hacker
- Manifeste du Cyborg https://fr.wikipedia.org/wiki/Manifeste_cyborg
- Manifeste Cypherpunk https://fr.wikipedia.org/wiki/Cypherpunk
- Manifeste Cyberpunk : http://cyberpunk.asia/pages_html.php?html=manifeste&lng=fr
- Manifeste GNU : https://www.gnu.org/gnu/manifesto.fr.html
- The Hippocratic License 2.1 : https://firstdonoharm.dev/
- A Code of Conduct for Open Source Projects : https://www.contributor-covenant.org/ par https://where.coraline.codes/
- Ethical Source: Open Source, Evolved : https://ethicalsource.dev/
- Guide pour les lanceurs d'alerte : https://transparency-france.org/actu/guide-pratique-pour-aider-les-lanceurs-dalerte/
- Le manifeste Agile : http://manifesto.softwarecraftsmanship.org/
- Le Serment Holberton-Turing :  https://www.holbertonturingoath.org/accueil
- https://www.codedbias.com

Livres sur la technologie
===========================

Classiques
-----------


- Le droit à la paresse, Paul Lafargue : http://www.rutebeuf.com/textes/lafargue01.html
- Partie sur la technologie du Capital, Karl Marx : https://readingcapitalsydney.wordpress.com/2010/03/15/chapter-15-machinery-and-large-scale-industry-sections-1-4-2/
- Zone Autonomes Temporaires et Utopies Pirates, Hakim Bey : https://fr.wikipedia.org/wiki/Zone_autonome_temporaire
- Critique de Hackim Bey par Mooray Bookchin : https://theanarchistlibrary.org/library/murray-bookchin-social-anarchism-or-lifestyle-anarchism-an-unbridgeable-chasm
- "Bullshit jobs" de David Graeber, 2018: https://fr.wikipedia.org/wiki/Bullshit_jobs http://www.editionslesliensquiliberent.fr/livre-Bullshit_Jobs-9791020906335-1-1-0-1.html


Thématique
------------

- Économie : En attendant les robots, Antonio Casilli : https://www.seuil.com/ouvrage/en-attendant-les-robots-antonio-a-casilli/9782021401882
- Tech et démocratie : "Technologies partout, démocratie nulle part" par Yaël Benayoun et Irénée Régnauld, octobre 2020 : https://www.eyrolles.com/Litterature/Livre/technologie-partout-democratie-nulle-part-9782364052024/ - critique par Stéphane Bortzmeyer : https://www.bortzmeyer.org/techno-partout-democratie-nulle-part.html
- Surveillance : A la trace, Olivier Tesquet http://www.premierparallele.fr/livre/a-la-trace
- L'Open Space m'a tuer, Thomas Zuber : https://www.babelio.com/livres/Des-Isnards-LOpen-Space-ma-tuer/92864
- Robotisés, rebelles, rejetés ? Maîtriser les nouvelles technologies, par Yves Lafargues Editions Ouvrières, Collection "Portes ouvertes", 1993: https://sisr.swissinformatics.org/robotises-rebelles-rejetes-yves-lasfargues-1993/


