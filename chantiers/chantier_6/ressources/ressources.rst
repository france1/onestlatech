.. index::
   pair: Ressources; Vision de la tech et éthique en entreprise

.. _ressources_chantier_6:

========================================================================
Ressources
========================================================================

.. toctree::
   :maxdepth: 5

   framapad/framapad
   designers_ethiques/designers_ethiques
   manifestes/manifestes
