.. index::
   pair: netiquette; Vision de la tech et éthique en entreprise

.. _netiquette:

========================================================================
netiquette
========================================================================


.. seealso::

   - https://tools.ietf.org/html/rfc1855
   - https://fr.wikipedia.org/wiki/N%C3%A9tiquette
   - https://histoiredutelechargement.wordpress.com/tag/sally-hambridge/

.. contents::
   :depth: 3


Introduction
==============

La nétiquette est une règle informelle, puis une charte qui définit les
règles de conduite et de politesse recommandées sur les premiers médias
de communication mis à disposition par Internet.

Il s'agit de tentatives de formalisation d'un certain contrat social pour Internet.

Le premier document officiel définissant les règles de la nétiquette
est la RFC 18551, rédigée par `Sally Hambridge <https://histoiredutelechargement.wordpress.com/tag/sally-hambridge/>` pour
l'Internet Engineering Task Force, et diffusée en octobre 1995.

D'autres documents font aussi autorité, comme Netiquette2 (Virginia Shea, 1994)
et The Net : Users guidelines and netiquette3 (Arlene Rinaldi, 1996).

S’il ne fallait retenir qu’une règle : Ce que vous ne feriez pas lors
d’une conversation réelle face à votre correspondant, ne prenez pas
l’Internet comme bouclier pour le faire.

À cette notion de courtoisie et de respect de l’autre viennent ensuite
se greffer des règles supplémentaires relatives aux spécificités de
plusieurs médias.

Ces règles n’ont cependant pas été actualisées pour couvrir les médias
plus récents (forums, wikis, blogs, vidéo-conférences, etc.), les
standards plus récents (Unicode, XML, etc.) ni les technologies plus
récentes (haut débit, VoIP, etc.).

