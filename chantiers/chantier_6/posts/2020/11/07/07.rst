.. post:: 2020-11-07
   :tags: Texte fondateur
   :category: Vision de la tech et éthique en entreprise
   :author: onestla.tech
   :location: France
   :language: fr

.. _posts_chantier_6_2020_11_07:

===================================================================
**Texte fondateur inspiré par des femmes**
===================================================================

.. contents::
   :depth: 3

Texte fondateur inspiré par des femmes
=========================================

J'aime beaucoup l'idée qu'on ait des textes fondateurs dont on
s'inspirerait qui soient écrits par des femmes !

le texte sur lequel on a travaillé avant de relancer le mouvement à la
rentrée pourrait aussi être une base de travail ? https://onestla.tech/publications/une-autre-tech-est-possible/

je vais prendre le temps de lire les différents manifestes et on tâche
de se faire un framapad qui nous permettra d'itérer sur ce qu'on a lu?

:autre lien: https://agilemanifesto.org/iso/fr/manifesto.html
